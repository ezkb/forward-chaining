package fr.lirmm.graphik.graal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.api.io.Parser;
import fr.lirmm.graphik.graal.api.kb.KnowledgeBase;
import fr.lirmm.graphik.graal.chase_bench.io.ChaseBenchDataParser;
import fr.lirmm.graphik.graal.chase_bench.io.ChaseBenchRuleParser;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.core.ruleset.LinkedListRuleSet;
import fr.lirmm.graphik.graal.forward_chaining.rule_applier.RestrictedChaseRuleApplier;
import fr.lirmm.graphik.graal.io.dlp.DlgpWriter;
import fr.lirmm.graphik.graal.kb.DefaultKnowledgeBase;
import fr.lirmm.graphik.graal.kb.KBBuilderException;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.BasicChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined.ObliviousChase;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.DefaultEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.MultiThreadRuleApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.DefaultTriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.ConcurrentRestrictedTriggerChecker;
import fr.lirmm.graphik.util.stream.IteratorAdapter;
import fr.lirmm.graphik.util.stream.IteratorException;

public class App2 
{
	private static String rootDir = "./data/";

	private static Scanner scan = new Scanner(System.in);
	private static DlgpWriter writer = new DlgpWriter()
			;
    public static void main( String[] args ) throws KBBuilderException, ChaseException, IOException, AtomSetException, HomomorphismException, fr.lirmm.graphik.graal.api.forward_chaining.ChaseException 
    {
    	System.out.println("Create bases...");
		KnowledgeBase kb = createKB(args[0], args[1], args[2]);
		
		System.out.println("Duplicate bases...");
		AtomSet factsBase1 = kb.getFacts();
		AtomSet factsBase2 = new DefaultInMemoryGraphStore();
		int nbVar = factsBase1.getVariables().size();
		for (int i = 0; i < nbVar; ++i)
		{
			factsBase2.getFreshSymbolGenerator().getFreshSymbol();
		}
		
		factsBase2.addAll(kb.getFacts());
		
		// Restricted
		Chase c1 = new BasicChase(
				kb.getOntology(),
				kb.getFacts(),
				new MultiThreadRuleApplier(new ConcurrentRestrictedTriggerChecker(), new DefaultTriggerApplier()),
				new DefaultEndOfStepTreatment(),
				new LinkedList<ChasePreTreatment>());
		/*BreadthFirstChase c1 = new ParallelRestrictedChase(
				kb.getOntology(),
				factsBase1);*/
		fr.lirmm.graphik.graal.api.forward_chaining.Chase c2 = new fr.lirmm.graphik.graal.forward_chaining.BreadthFirstChase(
				kb.getOntology(), factsBase2, 
				new RestrictedChaseRuleApplier<AtomSet>());
		
		/*RealTimeProfiler p = new RealTimeProfiler(System.out);
		((Profilable)c).setProfiler(p);*/
		boolean wait = false;
 
		Integer etape = 0;
		while (c1.hasNext() && c2.hasNext())
		{
				System.out.println("=== Étape "+etape.toString()+" ===");
				if (factsBase1 instanceof DefaultInMemoryGraphStore)
				{
					System.out.println(((DefaultInMemoryGraphStore) factsBase1).size()+" atomes.");
				}
			//writer.write(kb.getFacts());
			//writer.flush();
			
			/*if (factsBase1 instanceof DefaultInMemoryGraphStore)
			{
				System.out.println(((DefaultInMemoryGraphStore) factsBase1).size()+" atomes.");
			}*/
			/*if (factsBase2 instanceof DefaultInMemoryGraphStore)
			{
				System.out.println(((DefaultInMemoryGraphStore) factsBase2).size()+" atomes.");
			}*/
			
			if (wait)
				waitEntry();

			//System.out.println(factsBase1.getVariables().size());
			//System.out.println(factsBase2.getVariables().size());
			++etape;
			//p.start("saturation");
		//	System.out.println("Computation of the new chase...");
			c1.next();
			/*if (factsBase1 instanceof DefaultInMemoryGraphStore)
			{
				System.out.println(((DefaultInMemoryGraphStore) factsBase1).size()+" atomes.");
			}*/
			//System.out.println("Computation of the graal chase...");
			/*c2.next();
			if (factsBase2 instanceof DefaultInMemoryGraphStore)
			{
				System.out.println(((DefaultInMemoryGraphStore) factsBase2).size()+" atomes.");
			}
			System.out.println(factsBase1.getVariables().size());
			System.out.println(factsBase2.getVariables().size());*/
			//waitEntry();
			//p.stop("saturation");
			
			/*System.out.println("Computation of the comparison...");
			if ((new BacktrackHomomorphism()).exist(
						new DefaultConjunctiveQuery((InMemoryAtomSet)factsBase1),
						factsBase2))
			{
				if ((new BacktrackHomomorphism()).exist(
							new DefaultConjunctiveQuery((InMemoryAtomSet)factsBase2), 
							factsBase1))
				{
					System.out.println("The fact bases are equivalent.");
				}
				else 
				{
					System.out.println("graal tails new");
				}
			}
			else if (SmartHomomorphism.instance().exist(
						new DefaultConjunctiveQuery((InMemoryAtomSet)factsBase2), 
						factsBase1))
			{
				System.out.println("new tails graal.");
			}
			else 
			{
				System.out.println("The fact bases are not equivalent.");
			}*/
		}
		
		System.out.println("=== Étape "+etape.toString()+" ===");
		//writer.write(kb.getFacts());
		writer.flush();
		System.out.println("Terminé en "+etape.toString()+" étapes.");
		writer.close();

    }
	private static void waitEntry() throws IOException {
		writer.flush();
		System.out.println("Appuyer sur entrée pour continuer.");
		scan.nextLine();
	}
	
	public static KnowledgeBase createKB(String factsPath, String STPath, String rulePath) throws AtomSetException, FileNotFoundException, fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException, IteratorException
	{
		AtomSet atomSetDest;
		AtomSet atomSetDest2;
		RuleSet targetTgdsSet = new LinkedListRuleSet();
		{
			RuleSet stTgdsSet = new LinkedListRuleSet();
			AtomSet atomSetSrc = new DefaultInMemoryGraphStore();
			atomSetDest = new DefaultInMemoryGraphStore();

			Parser<Atom> dataParser = new ChaseBenchDataParser(new File(factsPath));
			atomSetSrc.addAll(dataParser);
			atomSetDest.addAll(atomSetSrc);
	
			Parser<Rule> ruleParser = new ChaseBenchRuleParser(new File(STPath));
			stTgdsSet.addAll(new IteratorAdapter<Rule>(ruleParser));
	
			ruleParser = new ChaseBenchRuleParser(new File(rulePath));
			targetTgdsSet.addAll(new IteratorAdapter<Rule>(ruleParser));
			/*targetTgdsSet.addAll(
					Rules.computeSinglePiece(
							new IteratorAdapter<Rule>(ruleParser)));*/
			
			Chase c = new ObliviousChase(stTgdsSet, atomSetDest);
			c.execute();
			atomSetDest.removeAll(atomSetSrc);
			/*atomSetDest2 = new DefaultInMemoryGraphStore();
			CloseableIterator<Atom> it = atomSetDest.iterator();
			while (it.hasNext())
			{
				Atom a = it.next();
				if (!atomSetSrc.contains(a))
				{
					atomSetDest2.add(a);
				}
			}*/
		}

		return new DefaultKnowledgeBase(atomSetDest, targetTgdsSet);
	}

}
