package fr.lirmm.graphik.graal.rule_redundancy;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.core.factory.DefaultSubstitutionFactory;
import fr.lirmm.graphik.graal.core.ruleset.LinkedListRuleSet;
import fr.lirmm.graphik.graal.homomorphism.SmartHomomorphism;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.ParallelChase;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.DefaultEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.halting_condition.LimitAtomsHaltingCondition;
import fr.lirmm.graphik.graal.new_forward_chaining.halting_condition.LimitStepsHaltingCondition;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.util.stream.CloseableIterator;

public class InterRuleRedundancyRemover{
	private DlgpParser parser;
	private RuleSet rules;
	private int initialRulesNumbers;
	private int finalRulesNumbers;
	private int stepLimit;
	private boolean print;
	private int stepMax;
	private int treated;
	private int advancement;
	private int atomLimit;

	private RuleSet deletedRules;
	private RuleSet undecidableRules;
	private Map<Rule, CloseableIterator<Substitution>> substitutions;
	private Map<Rule, RuleSet> rulesAppliedByRules;
	
	public InterRuleRedundancyRemover(RuleSet rules, int stepLimit, int atomLimit, Boolean print) throws RuleRedundancyException
	{
		this.rules = rules;
		this.initialRulesNumbers = rules.size();
		this.finalRulesNumbers = rules.size();
		this.stepLimit = stepLimit;
		this.atomLimit = atomLimit;
		this.advancement = 0;
		this.treated = 0;
		this.print = print;
		this.stepMax = this.stepLimit*this.rules.size();
		this.deletedRules = new LinkedListRuleSet();
		this.undecidableRules = new LinkedListRuleSet();
		this.rulesAppliedByRules = new HashMap<>();
		this.substitutions = new HashMap<>();
		remover();
	}
	
	public InterRuleRedundancyRemover(RuleSet rules, int stepLimit, int atomLimit) throws RuleRedundancyException
	{
		this.rules = rules;
		this.initialRulesNumbers = rules.size();
		this.finalRulesNumbers = rules.size();
		this.stepLimit = stepLimit;
		this.atomLimit = atomLimit;
		this.advancement = 0;
		this.treated = 0;
		this.stepMax = this.stepLimit*this.rules.size();
		this.deletedRules = new LinkedListRuleSet();
		this.undecidableRules = new LinkedListRuleSet();
		this.rulesAppliedByRules = new HashMap<>();
		this.substitutions = new HashMap<>();
		remover();
	}
	
	public InterRuleRedundancyRemover(RuleSet rules) throws RuleRedundancyException
	{
		this(rules, 5, 100000);
	}
	

	public InterRuleRedundancyRemover(String dlgpFileName) throws RuleRedundancyException 
	{
		super();

		this.rules = new LinkedListRuleSet();

		try 
		{
			parser = new DlgpParser(new File(dlgpFileName));
	
			while (parser.hasNext()) 
			{
				Object o = parser.next();
				if (o instanceof Rule) 
				{
					this.rules.add((Rule) o);
				}
			}
		}
		catch (Exception e)
		{
			throw new RuleRedundancyException("An error occured during the removing of redundant rules.", e);
		}

		parser.close();
		
		this.initialRulesNumbers = rules.size();
		this.stepLimit = 10;
		this.advancement = 0;
		this.treated = 0;
		this.print = true;
		this.deletedRules = new LinkedListRuleSet();
		this.undecidableRules = new LinkedListRuleSet();
		this.rulesAppliedByRules = new HashMap<>();
		this.substitutions = new HashMap<>();
		remover();

	}
	
	public DlgpParser getParser() {
		return parser;
	}

	public void setParser(DlgpParser parser) {
		this.parser = parser;
	}

	public RuleSet getRules() {
		return rules;
	}

	public void setRules(RuleSet rules) {
		this.rules = rules;
	}

	public Integer getInitialRulesNumbers() {
		return initialRulesNumbers;
	}

	public void setInitialRulesNumbers(Integer initialRulesNumbers) {
		this.initialRulesNumbers = initialRulesNumbers;
	}

	public Integer getFinalRulesNumbers() {
		return finalRulesNumbers;
	}

	public void setFinalRulesNumbers(Integer finalRulesNumbers) {
		this.finalRulesNumbers = finalRulesNumbers;
	}

	public int getTreated() {
		return this.treated;
	}

	public void setProgress(int treated) {
		this.treated = treated;
	}

	public int getStepLimit() {
		return stepLimit;
	}

	public void setStepLimit(int stepLimit) {
		this.stepLimit = stepLimit;
	}

	/**
	 * @param rule
	 * @return
	 * @throws RuleRedundancyException
	 */
	private boolean isRedundant(Rule rule) throws RuleRedundancyException
	{
		try 
		{
			boolean isRed = false;

			DefaultInMemoryGraphStore factBase = new DefaultInMemoryGraphStore();
			
			factBase.addAll(rule.getBody());

			RuleApplierForRuleRedundancy ruleApplier = new RuleApplierForRuleRedundancy();
			ParallelChase chase = new ParallelChase(
					rules,
					factBase,
					ruleApplier,
					new DefaultEndOfStepTreatment(),
					new LinkedList<ChasePreTreatment>());
			
		    chase.addBFCHaltingCondition(new LimitStepsHaltingCondition(stepLimit));
		    chase.addBFCHaltingCondition(new LimitAtomsHaltingCondition(atomLimit));
		    
			Substitution sub = DefaultSubstitutionFactory.instance().createSubstitution();
			
			Set<Variable> frontier = rule.getFrontier();
			for (Variable var : frontier) {
				sub.put(var, var);
			}
		    
		    while(chase.hasNext() && !isRed)
		    {
		    	
		    	chase.next();  	
				isRed = SmartHomomorphism.instance().exist(DefaultConjunctiveQueryFactory.instance().create(rule.getHead()), factBase, sub);
				
		    }
		      
		    if(chase.getStep() == stepLimit)
		    	undecidableRules.add(rule);
		    else {
		    	rulesAppliedByRules.put(rule, ruleApplier.getRulesApplied());
			    substitutions.put(rule, SmartHomomorphism.instance().execute(DefaultConjunctiveQueryFactory.instance().create(rule.getHead()), factBase, sub));
		    }
		    
		    advancement += chase.getStep();
		    
		    stepLimit = (int)Math.ceil((double)(stepMax - advancement)/((double)(initialRulesNumbers - treated)));
		 
			return isRed;
		}
		catch (Exception e)
		{
			throw new RuleRedundancyException("An error occured during the removing of redundant rules.", e);
		}
	}

	public boolean isPrint() {
		return print;
	}

	public void setPrint(boolean print) {
		this.print = print;
	}

	public int getStepMax() {
		return stepMax;
	}

	public void setStepMax(int stepMax) {
		this.stepMax = stepMax;
	}

	public int getAdvancement() {
		return advancement;
	}

	public void setAdvancement(int advancement) {
		this.advancement = advancement;
	}

	public RuleSet getDeletedRules() {
		return deletedRules;
	}

	public void setDeletedRules(RuleSet deletedRules) {
		this.deletedRules = deletedRules;
	}

	public RuleSet getUndecidableRules() {
		return undecidableRules;
	}

	public void setUndecidableRules(RuleSet undecidableRules) {
		this.undecidableRules = undecidableRules;
	}

	public void setInitialRulesNumbers(int initialRulesNumbers) {
		this.initialRulesNumbers = initialRulesNumbers;
	}

	public void setFinalRulesNumbers(int finalRulesNumbers) {
		this.finalRulesNumbers = finalRulesNumbers;
	}

	public void setTreated(int treated) {
		this.treated = treated;
	}

	private void remover() throws RuleRedundancyException
	{
		RuleSet ruleSet = new LinkedListRuleSet(rules);

		if(this.print)
			System.out.println("Start Remover : RuleSetRedundancy");


		for (Rule rule : ruleSet)
		{
			rules.remove(rule);
			if(!isRedundant(rule))
			{
				rules.add(rule);
			}
			else{
				deletedRules.add(rule);
			}
			
			double progress = (double) (((double)++treated/(double)initialRulesNumbers)*100);
			
			if(this.print)
			{
				System.out.printf("%.2f",progress);
				System.out.println(" % ");
			}
			
		}
		
		finalRulesNumbers = rules.size();
	}
	
	public InterRuleRedundancyRemover() {
		super();
	}

	public RuleSet getRuleSet() {
		return rules;
	}

	public void setRuleSet(RuleSet rules) {
		this.rules = rules;
	}
	
	public RuleSet getDeletedRuleSet() {
		return this.deletedRules;
	}

	public Map<Rule, RuleSet> getRulesAppliedByRules() {
		return this.rulesAppliedByRules;
	}

	public Map<Rule, CloseableIterator<Substitution>> getSubstitutions() {
		return this.substitutions;
	}

}