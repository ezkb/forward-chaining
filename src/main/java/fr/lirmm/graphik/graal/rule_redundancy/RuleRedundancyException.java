package fr.lirmm.graphik.graal.rule_redundancy;

public class RuleRedundancyException extends Exception 
{
	private static final long serialVersionUID = -7603011474373459478L;

	public RuleRedundancyException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public RuleRedundancyException(String message) 
	{
		super(message);
	}
	
	public RuleRedundancyException (Throwable e) 
	{
		super(e);
	}
}
