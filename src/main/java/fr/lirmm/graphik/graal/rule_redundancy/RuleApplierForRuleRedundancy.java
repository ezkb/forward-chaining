package fr.lirmm.graphik.graal.rule_redundancy;

import java.util.Set;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.core.ruleset.LinkedListRuleSet;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.RuleApplierException;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DefaultRuleApplier;

public class RuleApplierForRuleRedundancy extends DefaultRuleApplier {
	
	private RuleSet rulesApplied;
	
	public RuleApplierForRuleRedundancy()
	{
		super();
		rulesApplied = new LinkedListRuleSet();
	}
	
	public RuleSet getRulesApplied() {
		return rulesApplied;
	}

	public void setRulesApplied(RuleSet rulesApplied) {
		this.rulesApplied = rulesApplied;
	}

	@Override
	public void parallelApply(Rule r, AtomSet checkedFactBase, AtomSet precNewFacts, AtomSet newFacts) throws RuleApplierException 
	{
		Set<Substitution> homo;
		try 
		{
			if (this.getPreComputedTriggers().containsKey(r))
			{
				homo = this.getPreComputedTriggers(r);
			}
			else
			{
				homo = getNewTriggers(r, precNewFacts);
			}
		
			for (Substitution s : homo)
			{
				if (this.getTriggerChecker().check(r, s, checkedFactBase, true))
				{
					rulesApplied.add(r);
					this.getTriggerApplier().parallelApply(r, s, newFacts);
				}
			}
		}
		catch (Exception e) 
		{
			throw new RuleApplierException("An error occured during the rule application.", e);
		}
	}
	

}
