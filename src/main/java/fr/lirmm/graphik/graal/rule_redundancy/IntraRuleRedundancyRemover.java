package fr.lirmm.graphik.graal.rule_redundancy;


import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.core.DefaultRule;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.core_algorithm.Core;
import fr.lirmm.graphik.graal.core_algorithm.NaiveCore;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;

public class IntraRuleRedundancyRemover {

	private String dlgpRule;
	private Rule rule;
	
	public IntraRuleRedundancyRemover() {
		super();

	}
	
	public IntraRuleRedundancyRemover(String dlgpRule) throws RuleRedundancyException 
	{
		super();
		this.dlgpRule = dlgpRule;
		try 
		{
			rule = DlgpParser.parseRule(this.dlgpRule);
		}
		catch (Exception e)
		{
			throw new RuleRedundancyException("An error occured during the removing of redundancy in the rule : "+rule, e);
		}
		remover();
	}
	
	public IntraRuleRedundancyRemover(Rule rule) throws RuleRedundancyException
	{
		super();
		this.rule = rule;
		remover();
	}
	
	private void bodyRedundanciesRemover() throws RuleRedundancyException
	{
		try
		{
			Core core = new NaiveCore(rule.getFrontier());
			
			String label = rule.getLabel();	
			
			rule = new DefaultRule(label, core.getCore(rule.getBody()), rule.getHead());
		}
		catch(Exception e)
		{
			throw new RuleRedundancyException("An error occured during the removing of redundancy in the rule : "+rule, e);
		}
				
	}
	
	private void headToBodyHeadRemover() throws RuleRedundancyException
	{
		try
		{
			String label = rule.getLabel();
			
			CloseableIteratorWithoutException<Atom> iterator = rule.getBody().iterator();
			
			InMemoryAtomSet atomSet = new DefaultInMemoryGraphStore(), result, body = rule.getBody(), head = new DefaultInMemoryGraphStore();
			atomSet.addAll(rule.getBody());
			atomSet.addAll(rule.getHead());
			
			Core core = new NaiveCore(rule.getBody().getVariables());
			result = core.getCore(atomSet);		
			
			iterator = result.iterator();
			
			while(iterator.hasNext()) {
				Atom a = (Atom) iterator.next();
				if(!rule.getBody().contains(a))
					head.add(a);
			}
			if(!head.isEmpty())
				rule = new DefaultRule(label, body, head);
			else
				rule = new DefaultRule();
		}
		catch (Exception e)
		{
			throw new RuleRedundancyException("An error occured during the removing of redundancy in the rule : "+rule, e);
		}
	}
	
	private void remover() throws RuleRedundancyException
	{
		bodyRedundanciesRemover();
		
		headToBodyHeadRemover();
	}

	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}
	

}

