package fr.lirmm.graphik.graal;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;

import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.api.core.RuleSetException;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.core.ruleset.LinkedListRuleSet;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.graal.io.dlp.DlgpWriter;
import fr.lirmm.graphik.graal.rule_redundancy.InterRuleRedundancyRemover;
import fr.lirmm.graphik.graal.rule_redundancy.IntraRuleRedundancyRemover;
import fr.lirmm.graphik.graal.rule_redundancy.RuleRedundancyException;
import fr.lirmm.graphik.util.stream.CloseableIterator;

public class MainRedundancy 
{
	static Writer outInfo;
	static int stepLimit;
	static int atomLimit;
	static int choice;
	static boolean print;
	static double treatement;
	static int plannedSteps;

	
	public static void main(String[] args) throws IOException, RuleSetException, RuleRedundancyException, AtomSetException
	{
		boolean first = true, second = true, all = true;
		stepLimit = 5;
		atomLimit = 100000;
		print = true;
		plannedSteps = 0;
		choice = 3;

		switch(args.length)
		{
			case 0:
			case 1:
				System.err.println("Usage : java -jar simplifier.jar fileIn.dlp fileOut.dlp [mode(1/2/3)] [infos.txt] [stepsLimit] [AtomsLimit] [print progress(true/false)]");
				return;
			case 7:
				print = Boolean.parseBoolean(args[6]);
			case 6:
				atomLimit = Integer.parseInt(args[5]);
			case 5:
				stepLimit = Integer.parseInt(args[4]);
			case 4:
				outInfo =  new FileWriter(new File(args[3]));
			case 3:
				choice = Integer.parseInt(args[2]);
		}
	
		if(args.length <= 3)
		{
			outInfo =  new FileWriter(new File("infos.txt"));
		}
		
		final String ruleThenBR = args[0];
		final String out = args[1];
		
		treatement = 0.0;
		double step = 0.0;
		
		switch(choice)
		{
			case 0:
				break;
			case 1:
				second = false;
				all = false;
				break;
			case 2:
				first = false;
				all = false;
				break;
		}
		
		
		// We create a rule base from a file		
		
		RuleSet ruleBase = new LinkedListRuleSet(new DlgpParser(new File(ruleThenBR)));
		plannedSteps = ruleBase.size()*stepLimit + ruleBase.size();
		
		// We simplify the rules themselves in a new RuleBase
		RuleSet newRuleBase = new LinkedListRuleSet();

		printInfo("\nStart Simplifier\n");
		
		if(first || all)
		{
			
			printInfo("Phase 1\r\n" + 
					"	Number of rules to analyze : "+ruleBase.size()+"\r\n" + 
					"	***** Suppression of intra-rule redundancies *****");
			
			for (Iterator<Rule> it = ruleBase.iterator(); it.hasNext();)
			{
				Rule before = it.next();
				Rule rule = (new IntraRuleRedundancyRemover(before)).getRule();
				
				if(rule.getBody().isEmpty())
				{
					printInfo(before+"\n \t\t ->  Suppression of the rule (the head is entirely redundant with respect to body) \n");
				}
				else
				{
					
					if(!(rule.getBody().size() == before.getBody().size() 
							&& rule.getHead().size() == before.getHead().size())) 
					{
						String label = rule.getLabel();
						rule.setLabel(label+"_irr");
						
						if(rule.getBody().size() != before.getBody().size() 
								&& rule.getHead().size() == before.getHead().size()) 
						{
							printInfo(before+"\n \t\t ->  Suppression of redundancies in the body \n");
						}
						if(rule.getBody().size() == before.getBody().size() 
								&& rule.getHead().size() != before.getHead().size()) 
						{
							printInfo(before+"\n \t\t ->  Suppression of redundancies in the head \n");
						}
						if(rule.getBody().size() != before.getBody().size() 
								&& rule.getHead().size() != before.getHead().size()) 
						{
							printInfo(before+"\n \t\t ->  Suppression of redundancies in the head and the body \n");
						}
						
						printInfo(" \t\t -> "+rule+"\n");
					}
			
					newRuleBase.add(rule);
					
				}
				
				step++;
				
				treatement = (step / (2*ruleBase.size())) * 100;
					
			}
		}
		
		if(second && !first)
		{
			newRuleBase = ruleBase;
		}
		
		if(second || all)
		{
			
			printInfo("****************************************************** \r\n" + 
					"\r\nPhase 2 :\r\n" + 
					"	Number of remaining rules : "+newRuleBase.size()+"\r\n" + 
					"	Number maximal of breadth-first chase allowed by rules : "+stepLimit+"\r\n" + 
					"	Number maximal of atoms allowed by breadth-first chase : "+atomLimit);
			
			// We remove redundant rules
			plannedSteps = ruleBase.size()*stepLimit;	
			
			InterRuleRedundancyRemover redundancy = new InterRuleRedundancyRemover(newRuleBase, stepLimit, atomLimit, print); 
			
			Map<Rule, RuleSet> rulesAppliedByRules = redundancy.getRulesAppliedByRules();
			Map<Rule, CloseableIterator<Substitution>> substitutions = redundancy.getSubstitutions();
			
			treatement = treatement + (((double)redundancy.getTreated()/(double)redundancy.getInitialRulesNumbers())*100 / 2);
			
			
			

			
			printInfo("***** Suppression of inter-rule redundancies *****");
			
			for (Rule rule : redundancy.getDeletedRuleSet()) {
				
				printInfo(rule+"\n");
				printInfo("\t \t -> redundant rule with respect to others detected and suppressed \n");
				printInfo("\t \t - homomorphism : ");
				
				CloseableIterator<Substitution> subIt = substitutions.get(rule);
				
				while(subIt.hasNext())
				{
					printInfo("\t \t \t \t"+subIt.next().toString());
				}
				
				printInfo("\t \t - rules applied : ");
				
				for(Rule ruleApplied : rulesAppliedByRules.get(rule))
				{
					printInfo("\t \t \t \t"+ruleApplied);
				}
				
			}
			
			for(Rule rule : redundancy.getUndecidableRules())
			{
				printInfo(rule+"\n");
				printInfo("\t \t -> chase stopped at step limit without redundancy detection \n");
			}
			
			printInfo("\n****************************************************** \r\n" + 
					"\n");
			printInfo("Synthesis :\r\n" + 
					"	Number of rules in input file : "+ruleBase.size()+"\r\n"+
					"	Number of rules in output file : "+redundancy.getFinalRulesNumbers()+"\r\n"+
					"   Number of redundant rules within the set of rules "+(redundancy.getInitialRulesNumbers()-redundancy.getFinalRulesNumbers()));
		}else
		{
			
			printInfo("\n****************************************************** \r\n" + 
					"\n");
			printInfo("Synthesis :\r\n" + 
					"	Number of rules in input file : "+ruleBase.size()+"\r\n"+
					"	Number of rules in output file : "+newRuleBase.size()+"\r\n");
			
		}
		
		// We write the result in a new file
		DlgpWriter writer = new DlgpWriter(out);
		for(Rule rule : newRuleBase)
		{
			writer.write(rule);
		}
					
		writer.close();

		
	
		printInfo("The new ontology is in the file: "+out);
		if(args.length >= 4)
			printInfo("The information about treatments are in: "+args[3]);
		else
			printInfo("The information about treatments are in: infos.txt");
		
		
		
		outInfo.close();
	}
	
	public static void printInfo (String s) throws IOException
	{
		outInfo.write(s+"\n");
		if(print)
			System.out.println(s);
	}

}
