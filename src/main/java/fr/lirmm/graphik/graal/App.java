package fr.lirmm.graphik.graal;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.api.kb.KnowledgeBase;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.graal.io.dlp.DlgpWriter;
import fr.lirmm.graphik.graal.kb.KBBuilder;
import fr.lirmm.graphik.graal.kb.KBBuilderException;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.BasicChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.DefaultEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.RuleSplit;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DefaultRuleApplier;

public class App 
{
	private static String rootDir = "./data/";

	private static Scanner scan = new Scanner(System.in);
	private static DlgpWriter writer = new DlgpWriter()
			;
    public static void main( String[] args ) throws KBBuilderException, ChaseException, IOException, AtomSetException, HomomorphismException 
    {
    	KBBuilder kbb = new KBBuilder();
		kbb.addAll(new DlgpParser(new File(rootDir,"example.dlp")));
		KnowledgeBase kb = kbb.build();
		
		List<ChasePreTreatment> listCPT = new LinkedList<ChasePreTreatment>();
		listCPT.add(new RuleSplit());
		
		// Restricted
		Chase c = new BasicChase(
				kb.getOntology(),
				kb.getFacts(),
				new DefaultRuleApplier(),
				new DefaultEndOfStepTreatment(),
				new LinkedList<ChasePreTreatment>());
		/*BreadthFirstChase c1 = new ParallelRestricted
		
		/*RealTimeProfiler p = new RealTimeProfiler(System.out);
		((Profilable)c).setProfiler(p);*/
		boolean wait = true;
 
		Integer etape = 0;
		while (c.hasNext())
		{
			System.out.println("=== Étape "+etape.toString()+" ===");
			writer.write(kb.getFacts());
			writer.flush();
			
			if (kb.getFacts() instanceof DefaultInMemoryGraphStore)
			{
				System.out.println(((DefaultInMemoryGraphStore) kb.getFacts()).size()+" atomes.");
			}
			
			if (wait)
				waitEntry();
			
			++etape;
			//p.start("saturation");
			c.next();
			//p.stop("saturation");
		}
		
		System.out.println("=== Étape "+etape.toString()+" ===");
		writer.write(kb.getFacts());
		writer.flush();
		System.out.println("Terminé en "+etape.toString()+" étapes.");
		writer.close();

    }
	private static void waitEntry() throws IOException {
		writer.flush();
		System.out.println("Appuyer sur entrée pour continuer.");
		scan.nextLine();
	}

}
