package fr.lirmm.graphik.graal.benchmark;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;

public abstract class AbstractChaseBenchmark implements ChaseBenchmark 
{
	private String name;
	private long totalTimeLimit = 3600; // 1 hour
	private long stepTimeLimit = 3600/2; // half an hour
	private long numberFactsLimit = 1000000;
	private ChaseBenchmarkResult chaseResult;
	private long beginTime;
	private boolean stepTimeOut = false;
	private AtomSet factBase;
	
	public AbstractChaseBenchmark (AtomSet factBase)
	{
		this.factBase = factBase;
	}

	@Override
	public boolean goNext() throws ChaseException, AtomSetException 
	{
		return ((beginTime + totalTimeLimit) > (System.currentTimeMillis()/1000))
				&& (stepTimeOut == false)
				&& (factBase.size() < numberFactsLimit);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void next() throws InterruptedException 
	{
		Thread execNext = new Thread(getNextRunnable());
		execNext.start();
		execNext.join(Math.min(getStepTimeLimit()*1000, getRemainingTime()*1000));
		if (execNext.isAlive())
		{
		//	execNext.stop();
			execNext.join();
			setStepTimeOut(true);
		}
	}

	@Override
	public ChaseBenchmarkResult execute()
	{
		System.out.println("\nBeginning of the benchmark: "+getName());
		
		ChaseBenchmarkResult chaseResult = new ChaseBenchmarkResult(getName());
		beginTime = (long)System.currentTimeMillis()/1000;
		long startTime, stepTime;
		long step = 0;

		try 
		{
			chaseResult.addStepResult(++step, this.factBase.size(), 0);
		
			while (goNext()) 
			{
					System.out.println("Beginning of the step: "+step);
					startTime = (long)System.currentTimeMillis();
					next();
					stepTime = (long)System.currentTimeMillis() - startTime;
					System.out.println("End of the step "+step+" with "+factBase.size()+" atoms in the fact base"
							+ " and with an execution time of "+stepTime+" ms");
					
					if (!stepTimeOut)
						chaseResult.addStepResult(++step, this.factBase.size(), stepTime);
			}
		}
		catch (Exception e)
		{
			System.err.println("The benchmark "+getName()+" occured an error.");
			e.printStackTrace(System.err);
		}
		
		System.out.println("End of the benchmark: "+getName()+"\n");
		return chaseResult;
	}
	
	@Override
	public long getRemainingTime() 
	{
		return (long) (totalTimeLimit - (System.currentTimeMillis()/1000 - beginTime));
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public void setName(String name) 
	{
		this.name = name;
	}


	@Override
	public ChaseBenchmarkResult getChaseResult() 
	{
		return chaseResult;
	}

	@Override
	public long getTotalTimeLimit() 
	{
		return totalTimeLimit;
	}

	@Override
	public void setTotalTimeLimit(long totalTimeLimit) 
	{
		this.totalTimeLimit = totalTimeLimit;
	}

	@Override
	public long getStepTimeLimit() 
	{
		return stepTimeLimit;
	}

	@Override
	public void setStepTimeLimit(long stepTimeLimit) 
	{
		this.stepTimeLimit = stepTimeLimit;
	}

	@Override
	public long getNumberFactsLimit() 
	{
		return numberFactsLimit;
	}

	@Override
	public void setNumberFactsLimit(long numberFactsLimit) 
	{
		this.numberFactsLimit = numberFactsLimit;
	}

	public void setChaseResult(ChaseBenchmarkResult chaseResult) 
	{
		this.chaseResult = chaseResult;
	}

	public boolean isStepTimeOut() 
	{
		return stepTimeOut;
	}

	public void setStepTimeOut(boolean stepTimeOut) 
	{
		this.stepTimeOut = stepTimeOut;
	}

}
