package fr.lirmm.graphik.graal.benchmark;

import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;

public class NewChaseBenchmark extends AbstractChaseBenchmark 
{
	Chase chase;
	
	public NewChaseBenchmark (Chase c, String name)
	{
		super(c.getFactBase());
		setName(name);
		chase = c;
	}
	
	public NewChaseBenchmark (Chase c)
	{
		this(c, "New chase benchmark : "+c.getClass().getName());
	}
	
	@Override 
	public boolean goNext() throws ChaseException, AtomSetException
	{
		return super.goNext() && chase.hasNext();
	}
	
	private class ExecuteNext implements Runnable
	{
		@Override
		public void run() 
		{
			try 
			{
				chase.next();
			} 
			catch (ChaseException e) 
			{
				e.printStackTrace();
			}
		}
		
	}
	
	@Override
	public Runnable getNextRunnable()
	{
		return new ExecuteNext();
	}
}
