package fr.lirmm.graphik.graal.benchmark;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.api.forward_chaining.ChaseException;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.api.io.Parser;
import fr.lirmm.graphik.graal.api.kb.KnowledgeBase;
import fr.lirmm.graphik.graal.chase_bench.io.ChaseBenchDataParser;
import fr.lirmm.graphik.graal.chase_bench.io.ChaseBenchRuleParser;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.core.ruleset.LinkedListRuleSet;
import fr.lirmm.graphik.graal.forward_chaining.halting_condition.RestrictedChaseHaltingCondition;
import fr.lirmm.graphik.graal.forward_chaining.rule_applier.RestrictedChaseRuleApplier;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.graal.kb.DefaultKnowledgeBase;
import fr.lirmm.graphik.graal.kb.KBBuilder;
import fr.lirmm.graphik.graal.kb.KBBuilderException;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.BasicChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.DefaultBreadthFirstChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.ParallelChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined.ObliviousChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined.SemiObliviousChase;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.DefaultEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DefaultRuleApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.MultiThreadRuleApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.DefaultTriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.ConcurrentRestrictedTriggerChecker;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.ConcurrentSemiObliviousTriggerChecker;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.RestrictedTriggerChecker;
import fr.lirmm.graphik.util.stream.IteratorAdapter;

public class Benchmark2
{
	private static List<ChaseBenchmark> benchmarks = new LinkedList<ChaseBenchmark>();
	private static List<ChaseBenchmarkResult> benchmarkResults = new LinkedList<ChaseBenchmarkResult>();
	private static String prefixFileOut;

	public static void main(String[] args) throws KBBuilderException, AtomSetException, HomomorphismException, IOException, ChaseException, fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException, SQLException
	{
		if (args.length < 3)
		{
			System.err.println("Usage : java -jar benchmark.jar "
					+ "dataFolder stFolder rulesPath [prefixFileOut] [totalTimeLimit] [stepTimeLimit] [nbFactsLimit]");
			return;
		}
		
		prefixFileOut = "info";
		KnowledgeBase kb = createKB(args[0], args[1], args[2]);
		createBenchmarks(kb);
		
		switch(args.length)
		{
			default:
				benchmarks.forEach(bc -> bc.setNumberFactsLimit(Long.parseLong(args[6])));
			case 6:
				benchmarks.forEach(bc -> bc.setStepTimeLimit(Long.parseLong(args[5])));
			case 5:
				benchmarks.forEach(bc -> bc.setTotalTimeLimit(Long.parseLong(args[4])));
				break;
			case 4:
				prefixFileOut = args[3];
			case 3:
			case 2:
			case 1:
			case 0:
				// Do nothing
		}
		
		System.out.println("The execution of benchmarks is beginning...");
		while (!benchmarks.isEmpty())
		{
			ChaseBenchmark cb = benchmarks.remove(0);
			try 
			{
				benchmarkResults.add(cb.execute());
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		
		System.out.println("End of execution of benchmarks...");
		
		
		String tableOfNbAtomsFile = prefixFileOut+"_nb_facts.csv";
		CsvWriter nbFactsWriter = new CsvWriter(tableOfNbAtomsFile);
		System.out.println("Create the csv file with number of facts: "+tableOfNbAtomsFile);
		
		for(ChaseBenchmarkResult cbr : benchmarkResults)
		{
			nbFactsWriter.addColumn(
					cbr.getName(),
					cbr.getNbFactsColumn().stream().map(i -> Long.toString(i)).collect(Collectors.toList()));
		}
		nbFactsWriter.writeCsv();
		
		String tableOfExecutionTimesFile = prefixFileOut+"_execution_times.csv";
		CsvWriter ExecutionTimesWriter = new CsvWriter(tableOfExecutionTimesFile);
		System.out.println("Create the csv file with execution times : "+tableOfExecutionTimesFile);
		
		for(ChaseBenchmarkResult cbr : benchmarkResults)
		{
			ExecutionTimesWriter.addColumn(
					cbr.getName(),
					cbr.getTimeColumn().stream().map(i -> Long.toString(i)).collect(Collectors.toList()));
		}
		ExecutionTimesWriter.writeCsv();
		
		System.out.println("End of the benchmark !");
	}

	public static KnowledgeBase loadFile(String path) throws FileNotFoundException, KBBuilderException 
	{
		System.out.println("Building the knowledge base...");
		KBBuilder kbb = new KBBuilder();
		kbb.addAll(new DlgpParser(new File(path)));
		KnowledgeBase kb = kbb.build();
		return kb;
	}

	// Create different chases, redefine this method for testing with a different
	// set of chases
	public static void createBenchmarks(KnowledgeBase kb)
			throws AtomSetException, HomomorphismException, IOException, ChaseException, fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException, SQLException 
	{
		System.out.println("Building the benchmarks for all chases...");
		List<AtomSet> factBaseCopies = new LinkedList<AtomSet>();
		int nbCopies = 8;
		int nbVar = kb.getFacts().getVariables().size();
		for (int i = 0; i < nbCopies; ++i)
		{
			System.out.println(i);
			factBaseCopies.add(
			//new AdHocRdbmsStore(new HSQLDBDriver("test"+i, null))
					new DefaultInMemoryGraphStore()
			);
			factBaseCopies.get(i).addAll(kb.getFacts());
			for (int j = 0; j < nbVar; ++j)
			{
				factBaseCopies.get(i).getFreshSymbolGenerator().getFreshSymbol();
			}
		}
		
		// new chase algorithms
		benchmarks.add(new NewChaseBenchmark(new 
				BasicChase(kb.getOntology(), 
					factBaseCopies.remove(0),
					new MultiThreadRuleApplier(new ConcurrentRestrictedTriggerChecker(), new DefaultTriggerApplier()),
					new DefaultEndOfStepTreatment(),
					new LinkedList<ChasePreTreatment>()),
				"new Parallel Restricted Chase (MultiThreadRA)"));
		System.out.println("Building the benchmarks of new chases...");
		benchmarks.add(new NewChaseBenchmark(new SemiObliviousChase(kb.getOntology(), factBaseCopies.remove(0)),
				"new Semi Oblivious Chase (DefaultRA)"));
		benchmarks.add(new NewChaseBenchmark(new 
				ParallelChase(kb.getOntology(), 
						factBaseCopies.remove(0),
						new MultiThreadRuleApplier(new ConcurrentSemiObliviousTriggerChecker(), new DefaultTriggerApplier()),
						new DefaultEndOfStepTreatment(),
						new LinkedList<ChasePreTreatment>()),
				"new Semi Oblivious Chase (MultiThreadRA)"));
		benchmarks.add(new NewChaseBenchmark(new 
				DefaultBreadthFirstChase(kb.getOntology(), 
					factBaseCopies.remove(0),
					new DefaultRuleApplier(new RestrictedTriggerChecker(), new DefaultTriggerApplier()),
					new DefaultEndOfStepTreatment(),
					new LinkedList<ChasePreTreatment>()),
				"new BreadthFirst Restricted Chase (DefaultRuleApplier)"));
		benchmarks.add(new NewChaseBenchmark(new 
				ParallelChase(kb.getOntology(), 
					factBaseCopies.remove(0),
					new MultiThreadRuleApplier(new ConcurrentRestrictedTriggerChecker(), new DefaultTriggerApplier()),
					new DefaultEndOfStepTreatment(),
					new LinkedList<ChasePreTreatment>()),
				"new Parallel Restricted Chase (MultiThreadRA)"));
		/*benchmarks.add(new NewChaseBenchmark(new 
				ParallelChase(kb.getOntology(), 
					factBaseCopies.remove(0),
					new RestrictedRuleApplier(),
					new DefaultEndOfStepTreatment(),
					new LinkedList<ChasePreTreatment>()),
				"new Parallel Restricted Chase (RestrictedRA)"));*/
		benchmarks.add(new NewChaseBenchmark(new 
				ParallelChase(kb.getOntology(), 
					factBaseCopies.remove(0),
					new DefaultRuleApplier(new RestrictedTriggerChecker(), new DefaultTriggerApplier()),
					new DefaultEndOfStepTreatment(),
					new LinkedList<ChasePreTreatment>()),
				"new Parallel Restricted Chase (DefaultRA)"));
		
		// Graal chase algorithms
		System.out.println("Building the benchmarks of Graal chases...");
		AtomSet factBase = factBaseCopies.remove(0);
		/*benchmarks.add(new GraalChaseBenchmark(factBase, 
				new fr.lirmm.graphik.graal.forward_chaining.BreadthFirstChase(kb.getOntology(), factBase, 
						new DefaultRuleApplier<AtomSet>(new FrontierRestrictedChaseHaltingCondition())), 
				"Graal SemiOblivious Chase"));*/
		//factBase = factBaseCopies.remove(0);
		benchmarks.add(new GraalChaseBenchmark(factBase, 
				new fr.lirmm.graphik.graal.forward_chaining.BreadthFirstChase(kb.getOntology(), factBase, 
						new RestrictedChaseRuleApplier<AtomSet>()), 
				"Graal Restricted Chase (RestrictedChaseRA)"));
		factBase = factBaseCopies.remove(0);
		benchmarks.add(new GraalChaseBenchmark(factBase, 
				new fr.lirmm.graphik.graal.forward_chaining.BreadthFirstChase(kb.getOntology(), factBase, 
						new fr.lirmm.graphik.graal.forward_chaining.rule_applier.DefaultRuleApplier<AtomSet>(new RestrictedChaseHaltingCondition())), 
				"Graal Restricted Chase (DefaultRA)"));
	}
	
	public static KnowledgeBase createKB(String factsPath, String STPath, String rulePath) throws AtomSetException, FileNotFoundException, fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException
	{
		AtomSet atomSetDest;
		RuleSet targetTgdsSet = new LinkedListRuleSet();
		{
			RuleSet stTgdsSet = new LinkedListRuleSet();
			AtomSet atomSetSrc = new DefaultInMemoryGraphStore();
			atomSetDest = new DefaultInMemoryGraphStore();

			Parser<Atom> dataParser = new ChaseBenchDataParser(new File(factsPath));
			atomSetSrc.addAll(dataParser);
			atomSetDest.addAll(atomSetSrc);
	
			Parser<Rule> ruleParser = new ChaseBenchRuleParser(new File(STPath));
			stTgdsSet.addAll(new IteratorAdapter<Rule>(ruleParser));
	
			ruleParser = new ChaseBenchRuleParser(new File(rulePath));
			targetTgdsSet.addAll(new IteratorAdapter<Rule>(ruleParser));
			
			Chase c = new ObliviousChase(stTgdsSet, atomSetDest);
			c.execute();
			atomSetDest.removeAll(atomSetSrc);
		}

		return new DefaultKnowledgeBase(atomSetDest, targetTgdsSet);
	}
}
