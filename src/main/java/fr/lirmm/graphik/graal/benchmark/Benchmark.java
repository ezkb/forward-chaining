package fr.lirmm.graphik.graal.benchmark;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.forward_chaining.ChaseException;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.api.kb.KnowledgeBase;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.forward_chaining.halting_condition.FrontierRestrictedChaseHaltingCondition;
import fr.lirmm.graphik.graal.forward_chaining.rule_applier.ExhaustiveRuleApplier;
import fr.lirmm.graphik.graal.forward_chaining.rule_applier.RestrictedChaseRuleApplier;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.graal.kb.KBBuilder;
import fr.lirmm.graphik.graal.kb.KBBuilderException;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined.BreadthFirstRestrictedChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined.CoreChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined.LocalCoreChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined.ObliviousChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined.ParallelRestrictedChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined.SemiObliviousChase;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined.VacuumChase;

public class Benchmark
{
	private static List<ChaseBenchmark> benchmarks = new LinkedList<ChaseBenchmark>();
	private static List<ChaseBenchmarkResult> benchmarkResults = new LinkedList<ChaseBenchmarkResult>();
	private static String prefixFileOut;

	public static void main(String[] args) throws KBBuilderException, AtomSetException, HomomorphismException, IOException, ChaseException, fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException
	{
		if (args.length == 0)
		{
			System.err.println("Usage : java -jar benchmark.jar "
					+ "fileIn.dlp [prefixFileOut] [totalTimeLimit] [stepTimeLimit] [nbFactsLimit]");
			return;
		}
		
		prefixFileOut = args[0].substring(0, args[0].indexOf("."));
		KnowledgeBase kb = loadFile(args[0]);
		createBenchmarks(kb);
		
		switch(args.length)
		{
			default:
				benchmarks.forEach(bc -> bc.setNumberFactsLimit(Long.parseLong(args[4])));
			case 4:
				benchmarks.forEach(bc -> bc.setStepTimeLimit(Long.parseLong(args[3])));
			case 3:
				benchmarks.forEach(bc -> bc.setTotalTimeLimit(Long.parseLong(args[2])));
				break;
			case 2:
				prefixFileOut = args[1];
			case 1:
			case 0:
				// Do nothing
		}
		
		System.out.println("The execution of benchmarks is beginning...");
		while (!benchmarks.isEmpty())
		{
			ChaseBenchmark cb = benchmarks.remove(0);
			try 
			{
				benchmarkResults.add(cb.execute());
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		
		System.out.println("End of execution of benchmarks...");
		
		
		String tableOfNbAtomsFile = prefixFileOut+"_nb_facts.csv";
		CsvWriter nbFactsWriter = new CsvWriter(tableOfNbAtomsFile);
		System.out.println("Create the csv file with number of facts: "+tableOfNbAtomsFile);
		
		for(ChaseBenchmarkResult cbr : benchmarkResults)
		{
			nbFactsWriter.addColumn(
					cbr.getName(),
					cbr.getNbFactsColumn().stream().map(i -> Long.toString(i)).collect(Collectors.toList()));
		}
		nbFactsWriter.writeCsv();
		
		String tableOfExecutionTimesFile = prefixFileOut+"_execution_times.csv";
		CsvWriter ExecutionTimesWriter = new CsvWriter(tableOfExecutionTimesFile);
		System.out.println("Create the csv file with execution times : "+tableOfExecutionTimesFile);
		
		for(ChaseBenchmarkResult cbr : benchmarkResults)
		{
			ExecutionTimesWriter.addColumn(
					cbr.getName(),
					cbr.getTimeColumn().stream().map(i -> Long.toString(i)).collect(Collectors.toList()));
		}
		ExecutionTimesWriter.writeCsv();
		
		System.out.println("End of the benchmark !");
	}

	public static KnowledgeBase loadFile(String path) throws FileNotFoundException, KBBuilderException 
	{
		System.out.println("Building the knowledge base...");
		KBBuilder kbb = new KBBuilder();
		kbb.addAll(new DlgpParser(new File(path)));
		KnowledgeBase kb = kbb.build();
		return kb;
	}

	// Create different chases, redefine this method for testing with a different
	// set of chases
	public static void createBenchmarks(KnowledgeBase kb)
			throws AtomSetException, HomomorphismException, IOException, ChaseException, fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException 
	{
		System.out.println("Building the benchmarks for all chases...");
		List<AtomSet> factBaseCopies = new LinkedList<AtomSet>();
		int nbCopies = 9;
		for (int i = 0; i < nbCopies; ++i)
		{
			factBaseCopies.add(new DefaultInMemoryGraphStore());
			factBaseCopies.get(i).addAll(kb.getFacts());
		}
		
		// new chase algorithms
		System.out.println("Building the benchmarks of new chases...");
		benchmarks.add(new NewChaseBenchmark(new ObliviousChase(kb.getOntology(), factBaseCopies.remove(0)),
				"new Oblivious Chase"));
		benchmarks.add(new NewChaseBenchmark(new SemiObliviousChase(kb.getOntology(), factBaseCopies.remove(0)),
				"new Semi Oblivious Chase"));
		benchmarks.add(new NewChaseBenchmark(new ParallelRestrictedChase(kb.getOntology(), factBaseCopies.remove(0)),
				"new Parallel Restricted Chase"));
		benchmarks.add(new NewChaseBenchmark(new BreadthFirstRestrictedChase(kb.getOntology(), factBaseCopies.remove(0)),
				"new Breadth First Restricted Chase"));
		benchmarks.add(new NewChaseBenchmark(new LocalCoreChase(kb.getOntology(), factBaseCopies.remove(0)),
				"Local Core Chase"));
		benchmarks.add(new NewChaseBenchmark(new VacuumChase(kb.getOntology(), factBaseCopies.remove(0)),
				"Vacuum Chase"));
		benchmarks.add(new NewChaseBenchmark(new CoreChase(kb.getOntology(), factBaseCopies.remove(0)),
				"Core Chase"));
		
		// Graal chase algorithms
		System.out.println("Building the benchmarks of Graal chases...");
		AtomSet factBase = factBaseCopies.remove(0);
		benchmarks.add(new GraalChaseBenchmark(factBase, 
				new fr.lirmm.graphik.graal.forward_chaining.BreadthFirstChase(kb.getOntology(), factBase, 
						new ExhaustiveRuleApplier<AtomSet>(new FrontierRestrictedChaseHaltingCondition())), 
				"Graal SemiOblivious Chase"));
		factBase = factBaseCopies.remove(0);
		benchmarks.add(new GraalChaseBenchmark(factBase, 
				new fr.lirmm.graphik.graal.forward_chaining.BreadthFirstChase(kb.getOntology(), factBase, 
						new RestrictedChaseRuleApplier<AtomSet>()), 
				"Graal Parallel Restricted Chase"));
	}

}
