package fr.lirmm.graphik.graal.benchmark;

import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;

public interface ChaseBenchmark 
{
	String getName();
	void setName(String name);
	long getTotalTimeLimit();
	void setTotalTimeLimit(long limit);
	long getStepTimeLimit();
	void setStepTimeLimit(long limit);
	long getNumberFactsLimit();
	void setNumberFactsLimit(long limit);
	ChaseBenchmarkResult getChaseResult();
	boolean goNext() throws ChaseException, AtomSetException;
	void next() throws InterruptedException;
	long getRemainingTime();
	Runnable getNextRunnable();
	ChaseBenchmarkResult execute() throws InterruptedException;
}
