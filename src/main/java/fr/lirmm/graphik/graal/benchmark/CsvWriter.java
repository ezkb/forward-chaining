package fr.lirmm.graphik.graal.benchmark;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvWriter 
{
	List<List<String>> columns = new ArrayList<List<String>>();
	String fileName;
	String separator;
	
	public CsvWriter (String fileName, String separator)
	{
		this.fileName = fileName;
		this.separator = separator;
	}
	
	public CsvWriter (String fileName)
	{
		this(fileName, ";");
	}
	
	public void addColumn (String columName, List<String> column)
	{
		List<String> newColumn = new ArrayList<String>();
		newColumn.add(columName);
		newColumn.addAll(column);
		columns.add(newColumn);
	}
	
	public void writeCsv () throws IOException
	{
		FileWriter f = new FileWriter(fileName);
		
		int maxLine = columns.stream().map(c -> c.size()).max(Integer::compare).get();
		
		for (int i = 0; i < maxLine; ++i)
		{
			for (List<String> column : columns)
			{
				if (column.size() > i)
				{
					f.write(column.get(i));
				}
				f.write(separator);
			}
			f.write("\n");
		}
		
		f.close();
	}
}
