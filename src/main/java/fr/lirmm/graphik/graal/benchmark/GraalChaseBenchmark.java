package fr.lirmm.graphik.graal.benchmark;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.forward_chaining.Chase;
import fr.lirmm.graphik.graal.api.forward_chaining.ChaseException;

public class GraalChaseBenchmark extends AbstractChaseBenchmark 
{
	Chase chase;
	
	public GraalChaseBenchmark (AtomSet factBase, Chase c, String name)
	{
		super(factBase);
		setName(name);
		chase = c;
	}
	
	public GraalChaseBenchmark (AtomSet factBase, Chase c)
	{
		this(factBase, c, "Graal chase benchmark : "+c.getClass().getName());
	}
	
	@Override 
	public boolean goNext() throws fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException, AtomSetException
	{
		return super.goNext() && chase.hasNext();
	}
	
	private class ExecuteNext implements Runnable
	{
		@Override
		public void run() 
		{
			try 
			{
				chase.next();
			} 
			catch (ChaseException e) 
			{
				e.printStackTrace();
			}
		}
		
	}
	
	@Override
	public Runnable getNextRunnable()
	{
		return new ExecuteNext();
	}
}
