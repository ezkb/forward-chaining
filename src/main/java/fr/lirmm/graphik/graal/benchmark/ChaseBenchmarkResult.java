package fr.lirmm.graphik.graal.benchmark;

import java.util.ArrayList;
import java.util.List;

public class ChaseBenchmarkResult 
{
	private String name;
	private List<StepResult> results;

	public ChaseBenchmarkResult(String name) 
	{
		super();
		this.name = name;
		results = new ArrayList<>();
	}
	
	public List<Long> getNbFactsColumn()
	{
		List<Long> column = new ArrayList<Long>();
		
		for (StepResult sr : results)
		{
			column.add(sr.getnFacts());
		}
		
		return column;
	}
	
	public List<Long> getTimeColumn()
	{
		List<Long> column = new ArrayList<Long>();
		
		for (StepResult sr : results)
		{
			column.add(sr.getTime());
		}
		
		return column;
	}
	
	public long getTotalTime()
	{
		long total = 0;
		
		for (StepResult sr : results)
		{
			total += sr.getTime();
		}
		
		return total;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public void setResults(ArrayList<StepResult> results) 
	{
		this.results = results;
	}
	
	public void addStepResult(long nbStep, long nbFacts, long stepTime) 
	{
		results.add(new StepResult(nbStep, nbFacts, stepTime));
	}
	
	public class StepResult 
	{
		private long step;
		private long nFacts;
		private long time;
		
		public StepResult(long step, long nFacts, long time) 
		{
			this.step = step;
			this.nFacts = nFacts;
			this.time = time;
		}
		
		public long getStep() 
		{
			return step;
		}
		public void setStep(long step) 
		{
			this.step = step;
		}
		public long getnFacts() 
		{
			return nFacts;
		}
		public void setnFacts(long nFacts) 
		{
			this.nFacts = nFacts;
		}
		public long getTime() 
		{
			return time;
		}
		public void setTime(long time) 
		{
			this.time = time;
		}
		
	}
}
