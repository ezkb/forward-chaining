package fr.lirmm.graphik.graal.core_algorithm;

import java.io.IOException;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.util.stream.CloseableIterator;

public interface Core 
{
	public void computeCore(AtomSet a) throws AtomSetException, HomomorphismException, IOException;
	public DefaultInMemoryGraphStore getCore(AtomSet a) throws AtomSetException, HomomorphismException, IOException;
	public DefaultInMemoryGraphStore getCore(CloseableIterator<Atom> a) throws AtomSetException, HomomorphismException, IOException;
	public void setFrozenVariables(Substitution s);
	public void setFrozenVariables(Set<Variable> s);
	public void addFrozenVariables(Set<Variable> s);
	public void addFrozenVariable(Variable v);
	public void clearFrozenVariables();
	public void removeFrozenVariable(Variable v);
	public void removeFrozenVariables(Set<Variable> s);
}
