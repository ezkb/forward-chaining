package fr.lirmm.graphik.graal.core_algorithm;

import java.io.IOException;
import java.util.Comparator;
import java.util.Set;
import java.util.Vector;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core.DefaultConjunctiveQuery;
import fr.lirmm.graphik.graal.core.atomset.LinkedListAtomSet;
import fr.lirmm.graphik.graal.homomorphism.BacktrackHomomorphism;
import fr.lirmm.graphik.util.stream.CloseableIteratorAdapter;

public class ImprovedCore extends AbstractCore
{
	private static final Comparator<? super Variable> Variable = new VariableComparator();
	
	public ImprovedCore (Set<Variable> s) throws AtomSetException, HomomorphismException, IOException
	{
		this.setFrozenVariables(s);
	}
	
	public ImprovedCore (Substitution s) throws AtomSetException, HomomorphismException, IOException
	{
		this.setFrozenVariables(s);
	}
	
	public ImprovedCore () throws AtomSetException, HomomorphismException, IOException
	{
		this.initFrozenVariables();
	}
	
	public void computeCore (AtomSet a) throws AtomSetException, HomomorphismException, IOException
	{
		TypedAtomSet asCore = new TypedAtomSet(a.iterator());
		
		for (Variable v : this.frozenVariables.getTerms())
		{
			asCore.freezeVariable(v);
		}
		asCore.freeze();
		
		TypedAtomSet as;
		if (a instanceof TypedAtomSet)
			as = (TypedAtomSet)a;
		else 
			as = new TypedAtomSet(asCore);
		
		Vector<Variable> variables = new Vector<Variable>(asCore.getVariables());
		variables.sort(Variable);
		
		while(!variables.isEmpty())
		{
			Variable v = variables.lastElement();
			LinkedListAtomSet atomsUsingVar = 
					new LinkedListAtomSet(
							new CloseableIteratorAdapter<Atom>(asCore.getAtomsByTerm(v).iterator()));
			
			asCore.removeAll(atomsUsingVar);
			if ((new BacktrackHomomorphism()).exist(
							new DefaultConjunctiveQuery(atomsUsingVar), asCore, this.getFrozenVariables())
			)
			{
				if (!(new BacktrackHomomorphism()).exist(
						new DefaultConjunctiveQuery(as), asCore, this.getFrozenVariables()))
				{
					asCore.addAll(atomsUsingVar);
				}
				else
				{
					as.removeAll(atomsUsingVar);
					a.removeAll(asCore.getUnfrozenAtoms(atomsUsingVar));
				}
			}
			else
			{
				asCore.addAll(atomsUsingVar);
			}
			
			variables.remove(v);
		}
		
		asCore.unfreeze();
	}
}
