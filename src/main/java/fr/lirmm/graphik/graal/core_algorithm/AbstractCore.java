package fr.lirmm.graphik.graal.core_algorithm;

import java.io.IOException;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.core.factory.DefaultSubstitutionFactory;
import fr.lirmm.graphik.util.stream.CloseableIterator;

public abstract class AbstractCore implements Core 
{
	Substitution frozenVariables;
	
	protected Substitution getFrozenVariables ()
	{
		return frozenVariables;
	}
	
	@Override
	public void setFrozenVariables(Substitution s) 
	{
		frozenVariables = s;
	}

	@Override
	public void setFrozenVariables(Set<Variable> s) 
	{
		this.clearFrozenVariables();
		this.addFrozenVariables(s);
	}

	@Override
	public void addFrozenVariables(Set<Variable> s) 
	{
		for (Variable v : s)
		{
			this.frozenVariables.put(v, v);
		}
	}

	@Override
	public void addFrozenVariable(Variable v) 
	{
		this.frozenVariables.put(v, v);
	}

	@Override
	public void clearFrozenVariables() 
	{
		initFrozenVariables();
	}

	protected void initFrozenVariables() 
	{
		this.frozenVariables = DefaultSubstitutionFactory.instance().createSubstitution();
	}

	@Override
	public void removeFrozenVariable(Variable v) 
	{
		this.frozenVariables.remove(v);
	}

	@Override
	public void removeFrozenVariables(Set<Variable> s) 
	{
		for (Variable v : s)
		{
			this.frozenVariables.remove(v);
		}
	}

	@Override
	public DefaultInMemoryGraphStore getCore(AtomSet a) throws AtomSetException, HomomorphismException, IOException 
	{
		DefaultInMemoryGraphStore asCore = new DefaultInMemoryGraphStore();
		asCore.addAll(a.iterator());
		computeCore(asCore);
		return asCore;
	}

	@Override
	public DefaultInMemoryGraphStore getCore(CloseableIterator<Atom> a) throws AtomSetException, HomomorphismException, IOException 
	{
		DefaultInMemoryGraphStore core = new DefaultInMemoryGraphStore();
		
		while(a.hasNext())
		{
			core.add(a.next());
		}
		
		computeCore(core);
		
		return core;
	}

}
