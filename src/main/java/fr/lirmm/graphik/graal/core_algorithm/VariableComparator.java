package fr.lirmm.graphik.graal.core_algorithm;

import java.util.Comparator;

import fr.lirmm.graphik.graal.api.core.Variable;

public class VariableComparator implements Comparator<Variable> {

	public int compare(Variable o1, Variable o2) 
	{
		String l1 = o1.getLabel();
		String l2 = o2.getLabel();
		
		if (l1 == l2)
			return 0;
		
		if (l1.charAt(0) == 'E')
			l1 = l1.substring(2);
		else if (l1.charAt(0) == 'I')
			l1 = l1.substring(1);
		
		if (l2.charAt(0) == 'E')
			l2 = l2.substring(2);
		else if (l1.charAt(0) == 'I')
			l2 = l2.substring(1);
		
		try 
		{
			int l1Int = Integer.parseInt(l1);	
			int l2Int = Integer.parseInt(l2);
			
			if (l1Int > l2Int)
				return 1;
			else
				return -1;
		}
		catch (Exception e)
		{
			return o1.compareTo(o2);
		}
	}

}
