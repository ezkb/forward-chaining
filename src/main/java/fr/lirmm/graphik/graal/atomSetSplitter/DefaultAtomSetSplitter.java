package fr.lirmm.graphik.graal.atomSetSplitter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.IteratorException;

public class DefaultAtomSetSplitter implements AtomSetSplitter 
{
	private AtomSet toSplit;
	private Set<Pair<Variable,Variable>> edges; 
	private Map<Variable,Set<Variable>> components;
	private List<InMemoryAtomSet> pieces;
	private Map<Variable,AtomSet> variableToAtoms;
	
	@Override
	public List<InMemoryAtomSet> split(AtomSet toSplit) throws IteratorException, AtomSetException 
	{
		this.toSplit = toSplit;
		
		if (!(toSplit instanceof DefaultInMemoryGraphStore))
		{
			this.computeVariableToAtoms();
		}
		
		reduceToAtomConnectivityProblem();
		computeConnectivity();
		computePieces();
		
		return pieces;
	}

	private void reduceToAtomConnectivityProblem() throws IteratorException
	{
		edges = new HashSet<Pair<Variable,Variable>>();
		for (CloseableIterator<Atom> it = toSplit.iterator(); it.hasNext();)
		{
			Iterator<Variable> itVar = it.next().getVariables().iterator();
			if (itVar.hasNext())
			{
				Variable firstVar = itVar.next();
				
				if (!itVar.hasNext())
				{
					edges.add(Pair.of(firstVar, firstVar));
				}
				
				while (itVar.hasNext())
				{
					edges.add(Pair.of(firstVar, itVar.next()));
				}
			}
		}
	}
	
	private void computeConnectivity () throws AtomSetException
	{
		components = new HashMap<Variable,Set<Variable>>();
		for (Variable v : toSplit.getVariables())
		{
			components.put(v, new HashSet<Variable>());
			components.get(v).add(v);
		}
		
		for (Pair<Variable,Variable> edge : edges)
		{
			Set<Variable> x = components.get(edge.getLeft());
			Set<Variable> y = components.get(edge.getRight());

			if (x != y)
			{
				if (x.size() > y.size())
				{
					for (Variable v : y)
						components.put(v, x);
					x.addAll(y);
				}
				else
				{
					for (Variable v : x)
						components.put(v, y);
					y.addAll(x);
				}
			}
		}
	}
	
	private void computeVariableToAtoms() throws IteratorException, AtomSetException
	{
		variableToAtoms = new HashMap<Variable,AtomSet>();

		for (CloseableIterator<Atom> it = toSplit.iterator(); it.hasNext();)
		{
			Atom a = it.next();
			for (Variable v : a.getVariables())
			{
				if (!variableToAtoms.containsKey(v))
				{
					variableToAtoms.put(v, new DefaultInMemoryGraphStore());
				}
				variableToAtoms.get(v).add(a);
			}
		}
	}
	
	private AtomSet getAtomsByVariable(Variable v) throws IteratorException, AtomSetException
	{
		if (toSplit instanceof DefaultInMemoryGraphStore)
		{
			AtomSet atomSet = new DefaultInMemoryGraphStore();
			for (Atom a : ((DefaultInMemoryGraphStore)toSplit).getAtomsByTerm(v))
				atomSet.add(a);
			return atomSet;
		}
		else 
		{
			return variableToAtoms.containsKey(v) ? variableToAtoms.get(v) : new DefaultInMemoryGraphStore();
		}
	}
	
	private void computePieces () throws IteratorException, AtomSetException
	{
		this.pieces = new LinkedList<InMemoryAtomSet>();
		for (Set<Variable> s : (new HashSet<Set<Variable>>(this.components.values())))
		{
			InMemoryAtomSet atomSet = new DefaultInMemoryGraphStore();
			for (Variable v : s)
			{
				atomSet.addAll(this.getAtomsByVariable(v));
			}
			pieces.add(atomSet);
		}
	}
}
