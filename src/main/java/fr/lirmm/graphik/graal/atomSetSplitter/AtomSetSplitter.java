package fr.lirmm.graphik.graal.atomSetSplitter;

import java.util.List;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.util.stream.IteratorException;

public interface AtomSetSplitter 
{
	List<InMemoryAtomSet> split (AtomSet toSplit) throws IteratorException, AtomSetException;
}
