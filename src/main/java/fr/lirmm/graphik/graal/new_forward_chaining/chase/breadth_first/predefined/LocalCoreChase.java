package fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core_algorithm.Core;
import fr.lirmm.graphik.graal.core_algorithm.ImprovedCore;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.ParallelChase;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.LocalCoreEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ComputeCore;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.RuleSplit;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DefaultRuleApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.DefaultTriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.RestrictedTriggerChecker;

public class LocalCoreChase extends ParallelChase 
{
	public LocalCoreChase(Iterable<Rule> ruleBase, AtomSet factBase, Core core) throws ChaseException, AtomSetException 
	{
		super(ruleBase, factBase);
		this.setEndOfStepTreatment(new LocalCoreEndOfStepTreatment(factBase, core));
		this.setRuleApplier(new DefaultRuleApplier(new RestrictedTriggerChecker(), new DefaultTriggerApplier()));
		
		List<ChasePreTreatment> preTreatments = new LinkedList<ChasePreTreatment>();
		preTreatments.add(new RuleSplit());
		preTreatments.add(new ComputeCore(core));
		
		applyPreTreatments(preTreatments);
		init();
	}

	public LocalCoreChase(Iterable<Rule> ruleBase, AtomSet factBase) throws ChaseException, AtomSetException, HomomorphismException, IOException 
	{
		this(ruleBase, factBase, new ImprovedCore());
	}

}
