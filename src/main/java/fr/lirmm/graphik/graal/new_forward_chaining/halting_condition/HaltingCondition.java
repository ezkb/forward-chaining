package fr.lirmm.graphik.graal.new_forward_chaining.halting_condition;

import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;

public interface HaltingCondition {

	boolean isFinished();
	void init(Chase c);
}
