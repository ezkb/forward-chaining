package fr.lirmm.graphik.graal.new_forward_chaining.pretreatment;

import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;

public abstract class AbstractChasePreTreatment implements ChasePreTreatment 
{
	private Chase chase;
	
	@Override
	public void init(Chase c) 
	{
		chase = c;
	}
	
	protected Chase getChase()
	{
		return this.chase;
	}

}
