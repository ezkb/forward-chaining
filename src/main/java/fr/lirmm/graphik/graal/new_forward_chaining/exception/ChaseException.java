package fr.lirmm.graphik.graal.new_forward_chaining.exception;

public class ChaseException extends Exception 
{
	private static final long serialVersionUID = -6737246873013821636L;

	public ChaseException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public ChaseException(String message) 
	{
		super(message);
	}
	
	public ChaseException(Throwable e) 
	{
		super(e);
	}
}
