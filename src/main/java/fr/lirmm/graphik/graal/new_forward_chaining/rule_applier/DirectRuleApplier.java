package fr.lirmm.graphik.graal.new_forward_chaining.rule_applier;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.RuleApplierException;

public interface DirectRuleApplier extends RuleApplier
{
	void preComputeTriggers(Rule r, AtomSet precNewFacts) throws RuleApplierException;
	void directApply(Rule r, AtomSet checkedFactBase, AtomSet precNewFacts, AtomSet newFacts) throws RuleApplierException;
}
