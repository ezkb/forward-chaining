package fr.lirmm.graphik.graal.new_forward_chaining.rule_applier;

import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;

abstract public class AbstractRuleApplier implements RuleApplier
{
	private Chase chase;
	
	public void init(Chase c) 
	{
		chase = c;
	}
	
	protected Chase getChase()
	{
		return this.chase;
	}

	public void initNextStep()
	{
		// Do nothing
	}
}
