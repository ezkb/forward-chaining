package fr.lirmm.graphik.graal.new_forward_chaining.chase;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.core.ruleset.IndexedByBodyPredicatesRuleSet;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.EndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.halting_condition.HaltingCondition;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.RuleApplier;
import fr.lirmm.graphik.util.profiler.NoProfiler;
import fr.lirmm.graphik.util.profiler.Profiler;

public abstract class AbstractChase implements Chase
{
	private AtomSet factBase;
	private RuleSet ruleBase;
	private List<HaltingCondition> haltingConditions;
	private RuleApplier ruleApplier;
	private EndOfStepTreatment endOfStepTreatment;

	private Profiler profiler = NoProfiler.instance();
	
	public AbstractChase(Iterator<Rule> ruleBase, AtomSet factBase) throws ChaseException 
	{
		this.factBase = factBase;
		this.ruleBase = new IndexedByBodyPredicatesRuleSet();
		this.haltingConditions = new LinkedList<HaltingCondition>();
		initRules(ruleBase);
		
		for (HaltingCondition hc : haltingConditions)
		{
			hc.init(this);
		}
	}
	
	public AbstractChase(Iterable<Rule> ruleBase, AtomSet factBase) throws ChaseException 
	{
		this(ruleBase.iterator(), factBase);
	}
	
	private void initRules(Iterator<Rule> rules) 
	{
		while (rules.hasNext()) 
		{
			Rule r = rules.next();
			this.ruleBase.add(r);
		}
	}
	
	public AtomSet getFactBase() 
	{
		return factBase;
	}
	
	public RuleSet getRuleBase() 
	{
		return ruleBase;
	}
	
	@Override
	public void execute() throws ChaseException
	{
		if (this.getProfiler().isProfilingEnabled()) 
			this.getProfiler().start("saturation");
		try 
		{
			while (hasNext())
			{
				next();
			}
		}
		catch (Exception e)
		{
			throw new ChaseException("An error occured during the execution of the forward chaining.", e);
		}
		if (this.getProfiler().isProfilingEnabled()) 
			this.getProfiler().stop("saturation");
	}
	
	public void addHaltingCondition(HaltingCondition condition) 
	{
		this.haltingConditions.add(condition);
		condition.init(this);
	}
	
	@Override
	public void execute(HaltingCondition condition) throws ChaseException
	{
		addHaltingCondition(condition);
		execute();
	}
	
	@Override
	public boolean hasNext() throws ChaseException 
	{
		for (HaltingCondition halt : this.haltingConditions)
		{
			if (halt.isFinished())
				return false;
		}
		
		return true;
	}

	@Override
	public void setProfiler(Profiler profiler) 
	{
		this.profiler = profiler;
	}

	@Override
	public Profiler getProfiler() 
	{
		return profiler;
	}

	@Override
	public void setRuleBase(RuleSet ruleBase) throws ChaseException 
	{
		this.ruleBase = ruleBase;		
	}
	
	protected void applyPreTreatments (List<ChasePreTreatment> cpts) throws ChaseException
	{
		for (ChasePreTreatment cpt : cpts)
		{
			cpt.init(this);
			cpt.applyPretreatment();
		}
	}

	protected void setRuleApplier(RuleApplier ruleApplier) 
	{
		this.ruleApplier = ruleApplier;
		this.ruleApplier.init(this);
	}

	protected RuleApplier getRuleApplier() 
	{
		return ruleApplier;
	}

	public EndOfStepTreatment getEndOfStepTreatment() 
	{
		return endOfStepTreatment;
	}
	
	protected void setEndOfStepTreatment(EndOfStepTreatment endOfStepTreatment) 
	{
		this.endOfStepTreatment = endOfStepTreatment;
		this.endOfStepTreatment.init(this.getFactBase());
	}
}
