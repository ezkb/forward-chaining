package fr.lirmm.graphik.graal.new_forward_chaining.halting_condition;

import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;

public class TimeoutHaltingCondition extends AbstractHaltingCondition 
{
	private long timeout;
	private long startTime;
	
	public TimeoutHaltingCondition(long ms) {
		this.timeout = ms * 1000000l;
	}
	
	@Override
	public boolean isFinished() {
		long time = System.nanoTime();
		return (time - startTime) > timeout;
	}
	
	@Override
	public void init(Chase c) {
		super.init(c);
		startTime = System.nanoTime();
	}

}
