package fr.lirmm.graphik.graal.new_forward_chaining.rule_applier;

import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;

public interface RuleApplier 
{
	void init(Chase c);
	void initNextStep();
}
