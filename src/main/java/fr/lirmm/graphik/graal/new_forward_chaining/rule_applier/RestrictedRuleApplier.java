package fr.lirmm.graphik.graal.new_forward_chaining.rule_applier;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Query;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core.RuleWrapper2ConjunctiveQueryWithNegatedParts;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.homomorphism.SmartHomomorphism;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.RuleApplierException;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.TriggerCheckerException;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.DefaultTriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.TriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.RestrictedTriggerChecker;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.IteratorException;

public class RestrictedRuleApplier extends DefaultRuleApplier 
{
	public RestrictedRuleApplier (TriggerApplier ta)
	{
		super (new RestrictedTriggerChecker(), ta);
	}
	
	public RestrictedRuleApplier ()
	{
		this (new DefaultTriggerApplier());
	}
	
	@Override
	public void buildRuleQueries() 
	{
		Map<Rule,Query> ruleQueries = new HashMap<Rule,Query>();
		for (Rule r : this.getChase().getRuleBase())
		{
			ruleQueries.put(r, new RuleWrapper2ConjunctiveQueryWithNegatedParts(r));
		}
		this.setRuleQueries(ruleQueries);
	}

	@Override
	public void parallelApply(Rule r, AtomSet checkedFactBase, AtomSet precNewFacts, AtomSet newFacts) throws RuleApplierException 
	{
		Set<Substitution> homo;
		try 
		{
			if (getPreComputedTriggers().containsKey(r))
			{
				homo = getPreComputedTriggers (r);
			}
			else
			{
				homo = getNewTriggers(r, precNewFacts);
			}
			
			homo = getNewTriggers(r, precNewFacts);
		
			for (Substitution s : homo)
			{
				for (Variable t : r.getExistentials()) 
				{
					s.put(t, this.getChase().getFactBase().getFreshSymbolGenerator().getFreshSymbol());
				}
	
				getTriggerApplier().parallelApply(r, s, newFacts);
			}
		}
		catch (Exception e) 
		{
			throw new RuleApplierException("An error occured during the rule application.", e);
		}
	}
	
	@Override
	public void directApply(Rule r, AtomSet checkedFactBase, AtomSet precNewFacts, AtomSet newFacts) throws RuleApplierException 
	{
		Set<Substitution> homo;
		try 
		{
			if (getPreComputedTriggers().containsKey(r))
			{
				homo = getPreComputedTriggers (r);
			}
			else
			{
				homo = getNewTriggers(r, precNewFacts);
			}
			
			homo = getNewTriggers(r, precNewFacts);
		
			for (Substitution s : homo)
			{
				for (Variable t : r.getExistentials()) 
				{
					s.put(t, this.getChase().getFactBase().getFreshSymbolGenerator().getFreshSymbol());
				}
	
				getTriggerApplier().directApply(r, s, newFacts);
			}
		}
		catch (Exception e) 
		{
			throw new RuleApplierException("An error occured during the rule application.", e);
		}
	}
	
	@Override
	protected Set<Substitution> getNewTriggers(Rule r, AtomSet precNewFacts) throws IteratorException, AtomSetException, HomomorphismException, ChaseException, TriggerCheckerException
	{
		Set<Substitution> finalResult = new HashSet<Substitution>();
		Set<Substitution> partialS = new HashSet<Substitution>();
		boolean isLinear = r.getBody().size() == 1;
		
		CloseableIterator<Atom> it = r.getBody().iterator();
		 
		while (it.hasNext())
		{
			CloseableIterator<Substitution> sub = null;
			if (isLinear)
			{
				sub = SmartHomomorphism.instance().
						 execute(this.getRuleQueries().get(r), precNewFacts);
				it.next();
			}
			else
			{
				sub = SmartHomomorphism.instance().
						 execute(DefaultConjunctiveQueryFactory.instance().create(
								 it.next()), precNewFacts);
			}
			 
			while (sub.hasNext())
			{
				if (isLinear)
				{
					Substitution s = sub.next();
					if (this.getTriggerChecker().check(r, s, this.getChase().getFactBase(), true))
					{
						finalResult.add(s);
					}
				}
				else
				{
					partialS.add(sub.next());
				}
			}
		}
		 
		if (!isLinear)
		{
			for (Substitution s : partialS)
			{ 				 
				CloseableIterator<Substitution> sub = SmartHomomorphism.instance().
						 execute(this.getRuleQueries().get(r) ,this.getChase().getFactBase(), s);
				 
				while (sub.hasNext())
				{
					Substitution newSub = sub.next();
					for (Variable v : r.getFrontier())
					{
						newSub.put(v, s.createImageOf(v));
					}
					finalResult.add(newSub);
				}
				
				sub.close();
			}
		}
		 
		return finalResult;
	}
}
