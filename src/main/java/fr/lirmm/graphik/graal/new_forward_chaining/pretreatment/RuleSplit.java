package fr.lirmm.graphik.graal.new_forward_chaining.pretreatment;

import java.util.HashSet;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.core.Rules;
import fr.lirmm.graphik.graal.core.ruleset.LinkedListRuleSet;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;

public class RuleSplit extends AbstractChasePreTreatment 
{
	@Override
	public void applyPretreatment() throws ChaseException 
	{
		Set<Rule> newBase = new HashSet<Rule>();
		
		for (Rule r : this.getChase().getRuleBase())
		{
			try 
			{
				newBase.addAll(Rules.computeSinglePiece(r));
			} 
			catch (Exception e) 
			{
				throw new ChaseException("Exception in chase pretreatment.", e);
			}
		}

		this.getChase().setRuleBase(new LinkedListRuleSet(newBase.iterator()));
	}
}
