package fr.lirmm.graphik.graal.new_forward_chaining.chase;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Predicate;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.core.ruleset.IndexedByBodyPredicatesRuleSet;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.DefaultEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.EndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DefaultRuleApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DirectRuleApplier;

public class BasicChase extends AbstractChase 
{
	private Map<Rule,AtomSet> rulesToCheck;
	private IndexedByBodyPredicatesRuleSet indexedRuleSet;
	boolean hasNext = true;
	
	public BasicChase(Iterable<Rule> ruleBase, AtomSet factBase,
			DirectRuleApplier ra, EndOfStepTreatment endOfStepTreatment, 
			List<ChasePreTreatment> preTreatments) throws ChaseException 
	{
		super(ruleBase, factBase);
		
		this.setEndOfStepTreatment(endOfStepTreatment);
		this.setRuleApplier(ra);
		
		init();

		applyPreTreatments(preTreatments);
	}
	
	public BasicChase(Iterable<Rule> ruleBase, AtomSet factBase) throws ChaseException 
	{
		this(ruleBase, factBase, new DefaultRuleApplier(), new DefaultEndOfStepTreatment(), new LinkedList<ChasePreTreatment>());
	}
	
	private void init () throws ChaseException
	{
		initRulesToCheck();
	}
	
	private void initRulesToCheck() throws ChaseException
	{
		rulesToCheck = new HashMap<Rule,AtomSet>();
		indexedRuleSet = new IndexedByBodyPredicatesRuleSet();
		indexedRuleSet.addAll(this.getRuleBase().iterator());
		
		for (Rule r : this.indexedRuleSet)
		{
			if (!rulesToCheck.containsKey(r))
			{
				rulesToCheck.put(r, new DefaultInMemoryGraphStore());
			}
		}
		
		updateRulesToCheck(this.getFactBase());
	}
	
	protected void updateRulesToCheck (AtomSet newFacts) throws ChaseException
	{
		try 
		{
			for (Predicate p : newFacts.getPredicates()) 
			{
				Iterator<Rule> itR = indexedRuleSet.getRulesByBodyPredicate(p).iterator(); 
				
				while (itR.hasNext())
				{
					Rule rule = itR.next();
					if (!rulesToCheck.containsKey(rule))
					{
						rulesToCheck.put(rule, new DefaultInMemoryGraphStore());
					}
					
					rulesToCheck.get(rule).addAll(newFacts.atomsByPredicate(p));
				}
			}
		} 
		catch (Exception e)
		{
			throw new ChaseException("An error occured during the execution of the forward chaining.", e);
		}
	}

	public boolean hasNext() throws ChaseException 
	{
		try 
		{
			if (!hasNext)
				return false;
			
			if (!super.hasNext())
				return false;
		}
		catch (Exception e)
		{
			throw new ChaseException("An error occured during the execution of the forward chaining.", e);
		}
		
		return true;
	}
	
	@Override
	public void next() throws ChaseException 
	{
		try 
		{	
			DirectRuleApplier directRuleApplier = this.getRuleApplier();
			directRuleApplier.initNextStep();
			AtomSet newFacts = new DefaultInMemoryGraphStore();
			
			for (Rule r : this.indexedRuleSet)
			{
				AtomSet newFactsForThisRule = new DefaultInMemoryGraphStore();
				directRuleApplier.directApply(r, getFactBase(), this.rulesToCheck.get(r), newFactsForThisRule);
				this.updateRulesToCheck(newFactsForThisRule);
				newFacts.addAll(newFactsForThisRule);
				this.rulesToCheck.get(r).clear();
			}
			
			this.getEndOfStepTreatment().applyTreatment(newFacts);
			hasNext = !newFacts.isEmpty();
		}
		catch (Exception e)
		{
			throw new ChaseException("An error occured during the execution of the forward chaining.", e);
		}
	}

	protected DirectRuleApplier getRuleApplier() 
	{
		return (DirectRuleApplier)super.getRuleApplier();
	}
	
	protected void setRuleApplier(DirectRuleApplier ruleApplier)
	{
		super.setRuleApplier(ruleApplier);
	}
	
	@Override
	public void setRuleBase(RuleSet ruleBase) throws ChaseException 
	{
		super.setRuleBase(ruleBase);
		this.getRuleApplier().init(this);
		this.initRulesToCheck();
	}
}
