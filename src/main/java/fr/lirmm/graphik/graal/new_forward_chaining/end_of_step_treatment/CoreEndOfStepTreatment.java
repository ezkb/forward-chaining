package fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.core_algorithm.Core;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.EndOfStepTreatmentException;

public class CoreEndOfStepTreatment extends AbstractEndOfStepTreatment implements EndOfStepTreatment
{
	private Core core;
	
	public CoreEndOfStepTreatment (Core c)
	{
		core = c;
	}
	
	@Override
	public void applyTreatment(AtomSet newFacts) throws EndOfStepTreatmentException 
	{
		try 
		{
			core.computeCore(getFactBase());
		} 
		catch (Exception e) 
		{
			throw new EndOfStepTreatmentException("An exception occured during the global extension.", e);
		}
	}

}
