package fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined;

import java.util.LinkedList;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.ParallelChase;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.DefaultEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DefaultRuleApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.DefaultTriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.SemiObliviousTriggerChecker;

public class SemiObliviousChase extends ParallelChase 
{
	public SemiObliviousChase(Iterable<Rule> ruleBase, AtomSet factBase) throws ChaseException 
	{
		super(ruleBase, factBase,
				new DefaultRuleApplier(new SemiObliviousTriggerChecker(), new DefaultTriggerApplier()),
				new DefaultEndOfStepTreatment(),
				new LinkedList<ChasePreTreatment>());
	}
}
