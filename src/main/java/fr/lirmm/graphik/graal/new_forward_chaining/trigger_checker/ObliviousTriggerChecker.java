package fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.TriggerCheckerException;

public class ObliviousTriggerChecker implements TriggerChecker 
{
	@Override
	public boolean check(Rule r, Substitution h, AtomSet factBase, boolean paralle) throws TriggerCheckerException
	{
		return true;
	}
	
	@Override
	public void initNextStep()
	{
		// Nothing to do
	}
}
