package fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.EndOfStepTreatmentException;

public abstract class AbstractEndOfStepTreatment implements EndOfStepTreatment
{
	private AtomSet factBase;
	
	public void init (AtomSet factBase)
	{
		this.factBase = factBase;
	}
	
	public AtomSet getFactBase()
	{
		return factBase;
	}
	
	@Override
	public void globalExtend(AtomSet newFacts) throws EndOfStepTreatmentException
	{
		try 
		{
			this.getFactBase().addAll(newFacts);
		} 
		catch (AtomSetException e) 
		{
			throw new EndOfStepTreatmentException("An exception occured during the global extension.", e);
		}
		this.applyTreatment(newFacts);
	}
}
