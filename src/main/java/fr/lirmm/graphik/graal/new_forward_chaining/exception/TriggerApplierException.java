package fr.lirmm.graphik.graal.new_forward_chaining.exception;

public class TriggerApplierException extends Exception 
{
	private static final long serialVersionUID = 1L;

	public TriggerApplierException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public TriggerApplierException(String message) 
	{
		super(message);
	}
	
	public TriggerApplierException(Throwable e) 
	{
		super(e);
	}

}
