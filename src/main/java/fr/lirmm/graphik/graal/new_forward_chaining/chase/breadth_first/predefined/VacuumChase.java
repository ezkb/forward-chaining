package fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined;

import java.util.LinkedList;
import java.util.List;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.ParallelChase;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.VacuumEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.RuleSplit;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DefaultRuleApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.DefaultTriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.RestrictedTriggerChecker;

public class VacuumChase extends ParallelChase 
{
	public VacuumChase(Iterable<Rule> ruleBase, AtomSet factBase) throws ChaseException 
	{
		super(ruleBase, factBase,
				new DefaultRuleApplier(new RestrictedTriggerChecker(), new DefaultTriggerApplier()),
				new VacuumEndOfStepTreatment(),
				new LinkedList<ChasePreTreatment>());
		
		List<ChasePreTreatment> preTreatments = new LinkedList<ChasePreTreatment>();
		preTreatments.add(new RuleSplit());
		
		applyPreTreatments(preTreatments);
		init();
	}
}
