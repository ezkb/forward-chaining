package fr.lirmm.graphik.graal.new_forward_chaining.halting_condition;

import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.BreadthFirstChase;

abstract public class AbstractBFCHaltingCondition implements BFCHaltingCondition 
{
	private BreadthFirstChase chase;
	
	@Override
	public void init(BreadthFirstChase c) 
	{
		chase = c;
	}

	public BreadthFirstChase getChase() {
		return chase;
	}

}
