package fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.core_algorithm.Core;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.EndOfStepTreatmentException;

public class LocalCoreEndOfStepTreatment extends AbstractEndOfStepTreatment
{
	Core core;
	
	public LocalCoreEndOfStepTreatment (AtomSet factBase, Core c) throws AtomSetException
	{
		init(factBase);
		this.core = c;
		this.core.addFrozenVariables(getFactBase().getVariables());
	}
	
	@Override
	public void applyTreatment(AtomSet newFacts) throws EndOfStepTreatmentException 
	{
		try 
		{
			core.computeCore(getFactBase());
			core.addFrozenVariables(newFacts.getVariables());
		} 
		catch (Exception e) 
		{
			throw new EndOfStepTreatmentException("An exception occured during the local extension.", e);
		}
	}

}
