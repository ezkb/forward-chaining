package fr.lirmm.graphik.graal.new_forward_chaining.pretreatment;

import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;

public interface ChasePreTreatment 
{
	void init(Chase c);
	void applyPretreatment() throws ChaseException;
}
