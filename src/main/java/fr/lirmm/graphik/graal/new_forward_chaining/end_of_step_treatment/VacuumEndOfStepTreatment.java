package fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment;

import java.util.List;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.atomSetSplitter.AtomSetSplitter;
import fr.lirmm.graphik.graal.atomSetSplitter.DefaultAtomSetSplitter;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.core.factory.DefaultSubstitutionFactory;
import fr.lirmm.graphik.graal.homomorphism.BacktrackHomomorphism;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.EndOfStepTreatmentException;

public class VacuumEndOfStepTreatment extends AbstractEndOfStepTreatment
{
	private AtomSetSplitter splitter;
	
	public VacuumEndOfStepTreatment (AtomSetSplitter splitter)
	{
		this.splitter = splitter;
	}
	
	public VacuumEndOfStepTreatment ()
	{
		this(new DefaultAtomSetSplitter());
	}
	
	@Override
	public void globalExtend(AtomSet newFacts) throws EndOfStepTreatmentException
	{
		this.applyTreatment(newFacts);
		try 
		{
			this.getFactBase().addAll(newFacts);
		} 
		catch (AtomSetException e) 
		{
			throw new EndOfStepTreatmentException("An exception occured during the global extension.", e);
		}
	}

	@Override
	public void applyTreatment(AtomSet newFacts) throws EndOfStepTreatmentException 
	{
		List<InMemoryAtomSet> split = null;
		try 
		{
			split = splitter.split(getFactBase());
			Substitution s = DefaultSubstitutionFactory.instance().createSubstitution();
			for (Variable v : newFacts.getVariables())
			{
				s.put(v,v);
			}
		
			for (InMemoryAtomSet as : split)
			{
				ConjunctiveQuery query = DefaultConjunctiveQueryFactory.instance().create(as);
				BacktrackHomomorphism sh = new BacktrackHomomorphism();
				if (sh.exist(query, newFacts)) 
				{
					getFactBase().removeAll(as);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new EndOfStepTreatmentException("An exception occured during the global extension.", e);
		}
	}

}
