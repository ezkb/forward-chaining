package fr.lirmm.graphik.graal.new_forward_chaining.halting_condition;

import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.BreadthFirstChase;

public interface BFCHaltingCondition 
{
	boolean isFinished() throws AtomSetException;
	void init(BreadthFirstChase c);
}
