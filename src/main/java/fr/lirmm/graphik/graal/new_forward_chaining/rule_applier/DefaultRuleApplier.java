package fr.lirmm.graphik.graal.new_forward_chaining.rule_applier;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Query;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.homomorphism.SmartHomomorphism;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.RuleApplierException;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.TriggerCheckerException;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.DefaultTriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.TriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.RestrictedTriggerChecker;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.TriggerChecker;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.IteratorException;

public class DefaultRuleApplier extends AbstractRuleApplier implements ParallelRuleApplier, DirectRuleApplier
{		
	private Map<Rule,Query> ruleQueries = new HashMap<Rule,Query>();
	private TriggerChecker triggerChecker;
	private TriggerApplier triggerApplier;
	
	private Map<Rule,Set<Substitution>> preComputedTriggers = new HashMap<Rule,Set<Substitution>>();
	
	public DefaultRuleApplier(TriggerChecker tc, TriggerApplier ta)
	{
		this.triggerChecker = tc;
		this.triggerApplier = ta;
	}
	
	public DefaultRuleApplier()
	{
		this(new RestrictedTriggerChecker(), new DefaultTriggerApplier());
	}
	
	@Override
	public void init(Chase c) 
	{
		super.init(c);
		buildRuleQueries ();
		triggerApplier.init(this.getChase().getFactBase());
	}
	
	public void initNextStep()
	{
		triggerChecker.initNextStep();
		preComputedTriggers = new HashMap<Rule,Set<Substitution>>();
	}
	
	protected void buildRuleQueries ()
	{
		for (Rule r : this.getChase().getRuleBase())
		{
			this.ruleQueries.put(r, DefaultConjunctiveQueryFactory.instance().create(r.getBody(), 
					 new LinkedList<Term>(r.getFrontier())));
		}
	}

	@Override
	public void parallelApply(Rule r, AtomSet checkedFactBase, AtomSet precNewFacts, AtomSet newFacts) throws RuleApplierException 
	{
		Set<Substitution> homo;
		try 
		{
			if (preComputedTriggers.containsKey(r))
			{
				homo = preComputedTriggers.get(r);
			}
			else
			{
				homo = getNewTriggers(r, precNewFacts);
			}
		
			for (Substitution s : homo)
			{
				if (triggerChecker.check(r, s, checkedFactBase, true))
				{
					triggerApplier.parallelApply(r, s, newFacts);
				}
			}
		}
		catch (Exception e) 
		{
			throw new RuleApplierException("An error occured during the rule application.", e);
		}
	}
	
	protected Set<Substitution> getNewTriggers(Rule r, AtomSet precNewFacts) throws IteratorException, AtomSetException, HomomorphismException, ChaseException, TriggerCheckerException
	{
		Set<Substitution> finalResult = new HashSet<Substitution>();
		Set<Substitution> partialS = new HashSet<Substitution>();
		boolean isLinear = r.getBody().size() == 1;
		
		CloseableIterator<Atom> it = r.getBody().iterator();
		 
		while (it.hasNext())
		{
			CloseableIterator<Substitution> sub = null;
			if (isLinear)
			{
				sub = SmartHomomorphism.instance().
						 execute(DefaultConjunctiveQueryFactory.instance().create(it.next(), 
								 new LinkedList<Term>(r.getFrontier())), precNewFacts);
			}
			else
			{
				sub = SmartHomomorphism.instance().
						 execute(DefaultConjunctiveQueryFactory.instance().create(
								 it.next()), precNewFacts);
			}
			while (sub.hasNext())
			{
				partialS.add(sub.next());
			}
			 
			sub.close();
		}
		 
		it.close();
		 
		if (isLinear)
		{
			 finalResult = partialS;
		}
		else
		{
			for (Substitution s : partialS)
			{ 				 
				CloseableIterator<Substitution> sub = SmartHomomorphism.instance().
						 execute(ruleQueries.get(r) ,this.getChase().getFactBase(), s);
				 
				while (sub.hasNext())
				{
					Substitution newSub = sub.next();
					for (Variable v : r.getFrontier())
					{
						newSub.put(v, s.createImageOf(v));
					}
					finalResult.add(newSub);
				}
				 
				sub.close();
			}
		}
		 
		return finalResult;
	}
	
	protected Map<Rule, Query> getRuleQueries() 
	{
		return ruleQueries;
	}

	protected void setRuleQueries(Map<Rule, Query> ruleQueries) 
	{
		this.ruleQueries = ruleQueries;
	}

	@Override
	public void preComputeTriggers(Rule r, AtomSet precNewFacts) throws RuleApplierException 
	{
		try 
		{
			addPreComputedTriggers(r, this.getNewTriggers(r, precNewFacts));
		} 
		catch (Exception e) 
		{
			throw new RuleApplierException("An error occured during the rule application.", e);
		}
	}
	
	protected Set<Substitution> getPreComputedTriggers (Rule r)
	{
		return this.preComputedTriggers.get(r);
	}
	
	protected Map<Rule,Set<Substitution>> getPreComputedTriggers ()
	{
		return this.preComputedTriggers;
	}

	protected void addPreComputedTriggers (Rule r, Set<Substitution> s)
	{
		this.preComputedTriggers.put(r, s);
	}

	@Override
	public void directApply(Rule r, AtomSet checkedFactBase, AtomSet precNewFacts, AtomSet newFacts)
			throws RuleApplierException 
	{
		Set<Substitution> homo;
		try 
		{
			if (preComputedTriggers.containsKey(r))
			{
				homo = preComputedTriggers.get(r);
			}
			else
			{
				homo = getNewTriggers(r, precNewFacts);
			}
		
			for (Substitution s : homo)
			{
				if (triggerChecker.check(r, s, checkedFactBase, true))
				{
					triggerApplier.directApply(r, s, newFacts);
				}
			}
		}
		catch (Exception e) 
		{
			throw new RuleApplierException("An error occured during the rule application.", e);
		}
	}
	
	public TriggerChecker getTriggerChecker() 
	{
		return triggerChecker;
	}

	public void setTriggerChecker(TriggerChecker triggerChecker) 
	{
		this.triggerChecker = triggerChecker;
	}

	public TriggerApplier getTriggerApplier() 
	{
		return triggerApplier;
	}

	public void setTriggerApplier(TriggerApplier triggerApplier) 
	{
		this.triggerApplier = triggerApplier;
	}
}
