package fr.lirmm.graphik.graal.new_forward_chaining.halting_condition;

import fr.lirmm.graphik.graal.api.core.AtomSetException;

public class LimitAtomsHaltingCondition extends AbstractBFCHaltingCondition {
	long nbLimitAtoms;
	
	public LimitAtomsHaltingCondition(long steps) 
	{
		this.nbLimitAtoms = steps;
	}

	@Override
	public boolean isFinished() throws AtomSetException
	{
		return this.getChase().getFactBase().size() >= nbLimitAtoms;
		
	}
}
