package fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.TriggerApplierException;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;

public class DefaultTriggerApplier extends AbstractTriggerApplier
{
	@Override
	public void parallelApply(Rule rule, Substitution sub, AtomSet newFacts) throws TriggerApplierException 
	{
		try
		{
			addFreshSymbols(rule, sub);
			CloseableIteratorWithoutException<Atom> it = sub.createImageOf(rule.getHead()).iterator();

			while (it.hasNext()) 
			{
				newFacts.add(it.next());
			}
			
			it.close();
		}
		catch (Exception e) 
		{
			throw new TriggerApplierException("An exception occured during the applying of the trigger.", e);
		}
	}

	@Override
	public void directApply(Rule rule, Substitution sub) throws TriggerApplierException 
	{
		try
		{
			addFreshSymbols(rule, sub);
			CloseableIteratorWithoutException<Atom> it = sub.createImageOf(rule.getHead()).iterator();

			while (it.hasNext()) 
			{
				this.getFactBase().add(it.next());
			}
			
			it.close();
		}
		catch (Exception e) 
		{
			throw new TriggerApplierException("An exception occured during the applying of the trigger.", e);
		}
	}

	@Override
	public void directApply(Rule rule, Substitution sub, AtomSet newFacts) throws TriggerApplierException 
	{
		try
		{
			addFreshSymbols(rule, sub);
			CloseableIteratorWithoutException<Atom> it = sub.createImageOf(rule.getHead()).iterator();

			while (it.hasNext()) 
			{
				Atom a = it.next();
				newFacts.add(a);
				this.getFactBase().add(a);
			}
			
			it.close();
		}
		catch (Exception e) 
		{
			throw new TriggerApplierException("An exception occured during the applying of the trigger.", e);
		}
	}
}
