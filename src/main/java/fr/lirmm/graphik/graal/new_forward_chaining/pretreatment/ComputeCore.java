package fr.lirmm.graphik.graal.new_forward_chaining.pretreatment;

import java.io.IOException;

import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core_algorithm.Core;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;

public class ComputeCore extends AbstractChasePreTreatment 
{
	private Core core;
	
	public ComputeCore(Core c)
	{
		this.core = c;
	}
	
	@Override
	public void applyPretreatment() throws ChaseException 
	{
		try 
		{
			this.core.computeCore(this.getChase().getFactBase());
		} 
		catch (AtomSetException | HomomorphismException | IOException e) 
		{
			throw new ChaseException("Exception in computeCore pretreatment.", e);
		}
	}

}
