package fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first;

import java.util.LinkedList;
import java.util.List;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.DefaultEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.EndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DefaultRuleApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DirectRuleApplier;

public class DefaultBreadthFirstChase extends AbstractBreadthFirstChase
{
	public DefaultBreadthFirstChase(Iterable<Rule> ruleBase, AtomSet factBase,
			DirectRuleApplier ra, EndOfStepTreatment endOfStepTreatment, 
			List<ChasePreTreatment> preTreatments) throws ChaseException 
	{
		super(ruleBase, factBase, ra, endOfStepTreatment);

		applyPreTreatments(preTreatments);
	}
	
	public DefaultBreadthFirstChase(Iterable<Rule> ruleBase, AtomSet factBase) throws ChaseException 
	{
		this(ruleBase, factBase, new DefaultRuleApplier(), new DefaultEndOfStepTreatment(), new LinkedList<ChasePreTreatment>());
	}

	@Override
	public void next() throws ChaseException
	{
		try 
		{			
			++step;
			DirectRuleApplier directRuleApplier = this.getRuleApplier();
			directRuleApplier.initNextStep();
			for (Rule rule : this.getRulesToCheck())
			{
				directRuleApplier.preComputeTriggers(rule, getPrecNewFacts());
			}
			
			AtomSet newFacts = this.getNewFacts();
			
			for (Rule rule : this.getRulesToCheck())
			{
				directRuleApplier.directApply(rule, getFactBase(), getPrecNewFacts(), getNewFacts());
			}
			
			this.getEndOfStepTreatment().applyTreatment(newFacts);
			this.updateRulesToCheck();
	
			this.setPrecNewFacts(newFacts);
			this.setNewFacts(new DefaultInMemoryGraphStore());
		}
		catch (Exception e)
		{
			throw new ChaseException("An error occured during the execution of the forward chaining, during the step : "+this.getStep()+".", e);
		}
	}

	protected DirectRuleApplier getRuleApplier() 
	{
		return (DirectRuleApplier)super.getRuleApplier();
	}
	
	protected void setRuleApplier(DirectRuleApplier ruleApplier)
	{
		super.setRuleApplier(ruleApplier);
	}
}
