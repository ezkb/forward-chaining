package fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.EndOfStepTreatmentException;

public class DefaultEndOfStepTreatment extends AbstractEndOfStepTreatment
{
	@Override
	public void applyTreatment(AtomSet newFacts) throws EndOfStepTreatmentException 
	{
		// Do nothing
	}
}
