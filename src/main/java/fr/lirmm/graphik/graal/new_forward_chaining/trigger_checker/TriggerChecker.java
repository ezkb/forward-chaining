package fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.TriggerCheckerException;

public interface TriggerChecker 
{
	boolean check(Rule r, Substitution h, AtomSet factBase, boolean parallel) throws TriggerCheckerException;
	void initNextStep();
}
