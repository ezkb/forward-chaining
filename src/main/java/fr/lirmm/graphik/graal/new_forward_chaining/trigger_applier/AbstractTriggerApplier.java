package fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Variable;

public abstract class AbstractTriggerApplier implements TriggerApplier
{
	private AtomSet factBase;
	
	public void init (AtomSet factBase)
	{
		this.factBase = factBase;
	}
	
	public AtomSet getFactBase()
	{
		return factBase;
	}
	
	protected void addFreshSymbols(Rule r, Substitution s)
	{
		for (Variable t : r.getExistentials()) 
		{
			s.put(t, this.getFactBase().getFreshSymbolGenerator().getFreshSymbol());
		}
	}
}
