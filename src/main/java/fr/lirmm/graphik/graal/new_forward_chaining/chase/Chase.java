package fr.lirmm.graphik.graal.new_forward_chaining.chase;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.halting_condition.HaltingCondition;
import fr.lirmm.graphik.util.profiler.Profilable;

public interface Chase extends Profilable
{
	void execute() throws ChaseException;
	void execute(HaltingCondition condition) throws ChaseException;
	void next() throws ChaseException;
	boolean hasNext() throws ChaseException ;
	void addHaltingCondition(HaltingCondition haltingCondition);
	AtomSet getFactBase();
	RuleSet getRuleBase();
	void setRuleBase(RuleSet ruleSet) throws ChaseException;
}
