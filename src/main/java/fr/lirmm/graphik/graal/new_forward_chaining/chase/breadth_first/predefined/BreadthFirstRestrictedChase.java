package fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.predefined;

import java.util.LinkedList;
import java.util.List;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first.DefaultBreadthFirstChase;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.DefaultEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.ChasePreTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.pretreatment.RuleSplit;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DefaultRuleApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.DefaultTriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.RestrictedTriggerChecker;

public class BreadthFirstRestrictedChase extends DefaultBreadthFirstChase {

	public BreadthFirstRestrictedChase(Iterable<Rule> ruleBase, AtomSet factBase) throws ChaseException 
	{
		super(ruleBase, factBase);
		this.setEndOfStepTreatment(new DefaultEndOfStepTreatment());
		this.setRuleApplier(new DefaultRuleApplier(new RestrictedTriggerChecker(), new DefaultTriggerApplier()));
		
		List<ChasePreTreatment> preTreatments = new LinkedList<ChasePreTreatment>();
		preTreatments.add(new RuleSplit());
		
		applyPreTreatments(preTreatments);
		init();
	}

}
