package fr.lirmm.graphik.graal.new_forward_chaining.exception;

public class EndOfStepTreatmentException extends Exception 
{
	private static final long serialVersionUID = 7870575085665563439L;

	public EndOfStepTreatmentException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public EndOfStepTreatmentException(String message) 
	{
		super(message);
	}
	
	public EndOfStepTreatmentException(Throwable e) 
	{
		super(e);
	}
}
