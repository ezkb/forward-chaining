package fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker;

import java.util.HashMap;
import java.util.Map;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.homomorphism.SmartHomomorphism;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.TriggerCheckerException;

public class RestrictedTriggerChecker extends SemiObliviousTriggerChecker 
{
	Map<Rule,ConjunctiveQuery> queries = new HashMap<Rule,ConjunctiveQuery>();
	
	@Override
	public boolean check(Rule r, Substitution h, AtomSet factBase, boolean parallel) throws TriggerCheckerException 
	{
		if (!super.check(r, h, factBase, parallel))
			return false;
		
		try 
		{
			if (SmartHomomorphism.instance().exist(getQuery(r), factBase, h)) 
			{
				return false;
			}
		} 
		catch (Exception e) 
		{
			throw new TriggerCheckerException("An error occured while the restricted check.", e);
		}
		
		return true;
	}
	
	private ConjunctiveQuery getQuery(Rule r)
	{
		ConjunctiveQuery query = queries.get(r);
		
		if (query == null)
		{
			query = DefaultConjunctiveQueryFactory.instance().create(r.getHead());
			queries.put(r, query);
		}
		
		return query;
	}
}
