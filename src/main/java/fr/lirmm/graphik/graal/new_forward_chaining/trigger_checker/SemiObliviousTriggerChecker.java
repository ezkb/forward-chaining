package fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.TriggerCheckerException;

public class SemiObliviousTriggerChecker extends ObliviousTriggerChecker 
{
	Map<Rule,Set<List<Term>>> precFrontiers = new HashMap<Rule,Set<List<Term>>>();
	Map<Rule,Set<List<Term>>> currentStepFrontiers = new HashMap<Rule,Set<List<Term>>>();
	
	@Override
	public boolean check(Rule r, Substitution h, AtomSet factBase, boolean parallel) throws TriggerCheckerException 
	{
		List<Term> frontier = h.createImageOf(r.getFrontier());
		
		if (this.precFrontiers.containsKey(r) && this.precFrontiers.get(r).contains(frontier))
		{
			return false;
		}
		
		this.saveFrontier(r, frontier, parallel);
		
		return true;
	}
	
	public void saveFrontier (Rule r, List<Term> frontier, boolean parallel)
	{
		if (parallel)
		{
			if (!this.currentStepFrontiers.containsKey(r))
			{
				this.currentStepFrontiers.put(r, new HashSet<List<Term>>());
			}
			this.currentStepFrontiers.get(r).add(frontier);
		}
		else 
		{
			if (!this.precFrontiers.containsKey(r))
			{
				this.precFrontiers.put(r, new HashSet<List<Term>>());
			}
			this.precFrontiers.get(r).add(frontier);
		}
	}
	
	@Override
	public void initNextStep()
	{
		for (Map.Entry<Rule, Set<List<Term>>> e : this.currentStepFrontiers.entrySet())
		{
			if (this.precFrontiers.containsKey(e.getKey()))
			{
				this.precFrontiers.get(e.getKey()).addAll(e.getValue());
			}
			else 
			{
				this.precFrontiers.put(e.getKey(), e.getValue());
			}
		}
		currentStepFrontiers.clear();
	}
}
