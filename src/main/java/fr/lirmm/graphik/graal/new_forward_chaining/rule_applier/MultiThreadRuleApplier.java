package fr.lirmm.graphik.graal.new_forward_chaining.rule_applier;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.Semaphore;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Query;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.core.factory.DefaultSubstitutionFactory;
import fr.lirmm.graphik.graal.homomorphism.SmartHomomorphism;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.RuleApplierException;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.DefaultTriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier.TriggerApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.RestrictedTriggerChecker;
import fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker.TriggerChecker;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.IteratorException;

public class MultiThreadRuleApplier extends AbstractRuleApplier implements ParallelRuleApplier, DirectRuleApplier
{
	private Map<Rule,Query> ruleQueries = new HashMap<Rule,Query>();
	Semaphore lockTriggerApply = new Semaphore(1);
	
	boolean stop = false;
	Exception exception = null;
	
	private TriggerChecker triggerChecker;
	private TriggerApplier triggerApplier;
	
	private Map<Rule,Set<Substitution>> preComputedTriggers = new HashMap<Rule,Set<Substitution>>();
	
	public MultiThreadRuleApplier(TriggerChecker tc, TriggerApplier ta)
	{
		this.triggerChecker = tc;
		this.triggerApplier = ta;
	}
	
	public MultiThreadRuleApplier()
	{
		this(new RestrictedTriggerChecker(), new DefaultTriggerApplier());
	}
	
	@Override
	public void init(Chase c) 
	{
		super.init(c);
		buildRuleQueries ();
		triggerApplier.init(this.getChase().getFactBase());
	}
	
	public void initNextStep()
	{
		triggerChecker.initNextStep();
		preComputedTriggers = new HashMap<Rule,Set<Substitution>>();
	}
	
	@Override
	public void parallelApply(Rule r, AtomSet checkedFactBase, AtomSet precNewFacts, AtomSet newFacts)
			throws RuleApplierException 
	{
		try 
		{
			StreamApplyRule sar = new StreamApplyRule(
					r,
					checkedFactBase,
					precNewFacts,
					newFacts);
			sar.parallelApply ();
		} 
		catch (Exception e) 
		{
			throw new RuleApplierException("An error occured during the rule application.", e);
		}
		
		if (exception != null) 
		{
			throw new RuleApplierException("An error occured during the rule application.", exception);
		}
	}

	@Override
	public void directApply(Rule r, AtomSet checkedFactBase, AtomSet precNewFacts, AtomSet newFacts)
			throws RuleApplierException 
	{
		try 
		{
			StreamApplyRule sar = new StreamApplyRule(
					r,
					checkedFactBase,
					precNewFacts,
					newFacts);
			sar.directApply ();
		} 
		catch (Exception e) 
		{
			throw new RuleApplierException("An error occured during the rule application.", e);
		}
		
		if (exception != null) 
		{
			throw new RuleApplierException("An error occured during the rule application.", exception);
		}
	}

	@Override
	public void preComputeTriggers(Rule r, AtomSet precNewFacts) throws RuleApplierException 
	{
		try 
		{
			this.preComputedTriggers.put(r, (new StreamComputeTrigger(r, precNewFacts)).getTriggers());
		} 
		catch (Exception e) 
		{
			throw new RuleApplierException("An error occured during the rule application.", e);
		}
	}
	
	protected Set<Substitution> getPreComputedTriggers (Rule r)
	{
		return this.preComputedTriggers.get(r);
	}
	
	protected Map<Rule,Set<Substitution>> getPreComputedTriggers ()
	{
		return this.preComputedTriggers;
	}
	
	class StreamComputeTrigger
	{
		private Rule r;
		private AtomSet precNewFacts;
		private boolean isLinear;
		
		StreamComputeTrigger (Rule r, AtomSet precNewFacts) throws AtomSetException
		{
			this.r = r;
			this.precNewFacts = precNewFacts;
			this.isLinear = r.getBody().size() == 1;
		}
		
		public Stream<Substitution> getStreamTriggers ()
		{
			if (isLinear)
			{
				return beginStream()
					.map(a -> getPartialHomomorphisms(a))
					.flatMap(Function.identity())
					.distinct();
			}
			else 
			{
				return beginStream()
					.map(a -> getPartialHomomorphisms(a))
					.flatMap(Function.identity())
					.map(s -> extendHomomorphisms(s))
					.flatMap(Function.identity())
					.distinct();
			}
		}
		
		public Set<Substitution> getTriggers ()
		{
			return this.getStreamTriggers().collect(Collectors.toSet());
		}
		
		private Stream<Atom> beginStream ()
		{
			return iteratorToStream(
					new IteratorWrapper<Atom>(r.getBody().iterator()));
		}
		
		private Stream<Substitution> getPartialHomomorphisms(Atom a)
		{
			if (stop)
			{
				return Stream.empty();
			}
			
			try 
			{
				if (isLinear)
				{
					return iteratorToStream(
							new IteratorWrapper<Substitution>(
									SmartHomomorphism.instance().
									 execute((ConjunctiveQuery)ruleQueries.get(r),
											 precNewFacts)));
				}
				else 
				{
					return iteratorToStream(
							new IteratorWrapper<Substitution>(
									SmartHomomorphism.instance().
									 execute(DefaultConjunctiveQueryFactory.
											 instance().create(a), precNewFacts)));
				}
			} 
			catch (HomomorphismException e) 
			{
				stop = true;
				exception = e;
				return Stream.empty();
			}
		}
		
		private Stream<Substitution> extendHomomorphisms (Substitution s)
		{
			if (stop)
			{
				return Stream.empty();
			}
			
			Substitution frontierS = DefaultSubstitutionFactory.instance().createSubstitution();
			for (Variable v : r.getFrontier())
			{
				frontierS.put(v, s.createImageOf(v));
			}
			
			try 
			{
				return iteratorToStream(new ExtendedSubstitutionIterator(
						SmartHomomorphism.instance().
						 execute((ConjunctiveQuery)ruleQueries.get(r), 
								 getChase().getFactBase(), s),
						frontierS));
			} 
			catch (HomomorphismException e) 
			{
				stop = true;
				exception = e;
				return Stream.empty();
			}
		}
	}

	class StreamApplyRule
	{
		private Rule r;
		private AtomSet checkedFactBase;
		private AtomSet precNewFacts;
		private AtomSet newFacts;
		private boolean parallel;
		
		StreamApplyRule (
			Rule r,
			AtomSet checkedFactBase,
			AtomSet precNewFacts,
			AtomSet newFacts) throws AtomSetException
		{
			this.r = r;
			this.checkedFactBase = checkedFactBase;
			this.precNewFacts = precNewFacts;
			this.newFacts = newFacts;
		}
		
		private void parallelApply () throws AtomSetException
		{
			this.parallel = true;
			getStreamTriggers()
				.filter(s -> checkTrigger(s))
				.forEach(s -> parallelApplyTrigger(s));
		}
		
		private void directApply () throws AtomSetException
		{
			this.parallel = false;
			getStreamTriggers()
				.filter(s -> checkTrigger(s))
				.forEach(s -> directApplyTrigger(s));
		}
		
		public Stream<Substitution> getStreamTriggers() throws AtomSetException
		{
			if (getPreComputedTriggers().containsKey(r))
			{
				return getPreComputedTriggers(r).parallelStream();
			}
			else 
			{
				return (new StreamComputeTrigger(r, precNewFacts)).getStreamTriggers();
			}
		}
		
		private boolean checkTrigger (Substitution s)
		{
			if (stop)
			{
				return false;
			}
			
			try 
			{
				if (!parallel)
				{
					lockTriggerApply.acquire();
					boolean result = triggerChecker.check(r, s, checkedFactBase, parallel);
					lockTriggerApply.release();
					return result;
				}
				else 
				{
					return triggerChecker.check(r, s, checkedFactBase, parallel);
				}
			} 
			catch (Exception e) 
			{
				stop = true;
				exception = e;
				return false;
			}
		}
		
		private void parallelApplyTrigger (Substitution s) 
		{
			if (stop)
			{
				return;
			}
			
			try 
			{
				lockTriggerApply.acquire();
				triggerApplier.parallelApply(r, s, newFacts);
			}
			catch (Exception e) 
			{
				stop = true;
				exception = e;
				return;
			}
			
			lockTriggerApply.release();
		}
		
		private void directApplyTrigger (Substitution s) 
		{
			if (stop)
			{
				return;
			}
			
			try 
			{
				lockTriggerApply.acquire();
				triggerApplier.directApply(r, s, newFacts);
			}
			catch (Exception e) 
			{
				stop = true;
				exception = e;
				return;
			}
			
			lockTriggerApply.release();
		}
	}
	
	private <T> Stream<T> iteratorToStream (Iterator<T> it)
	{
		int characteristics = Spliterator.ORDERED | Spliterator.NONNULL;
		Spliterator<T> spliterator = Spliterators.spliteratorUnknownSize(
												it, characteristics);
		return StreamSupport.stream(spliterator, true);
	}
	
	protected void buildRuleQueries ()
	{
		for (Rule r : this.getChase().getRuleBase())
		{
			this.ruleQueries.put(r, DefaultConjunctiveQueryFactory.instance().create(r.getBody(), 
					 new LinkedList<Term>(r.getFrontier())));
		}
	}
	
	class IteratorWrapper<T> implements Iterator<T>
	{
		CloseableIterator<T> it; 
		
		public IteratorWrapper (CloseableIterator<T> it)
		{
			this.it = it;
		}

		@Override
		public boolean hasNext() 
		{
			try 
			{
				boolean hNext = !stop && it.hasNext();
				if (!hNext)
				{
					it.close();
				}
				return hNext;
			} 
			catch (IteratorException e) 
			{
				stop = true;
				exception = e;
				it.close();
				return false;
			}
		}

		@Override
		public T next() 
		{
			try 
			{
				return it.next();
			} 
			catch (IteratorException e) 
			{
				stop = true;
				exception = e;
			}
			
			return null;
		}
	}
	
	class ExtendedSubstitutionIterator extends IteratorWrapper<Substitution>
	{
		Substitution toPut;
		public ExtendedSubstitutionIterator(
				CloseableIterator<Substitution> it,
				Substitution toPut) 
		{
			super(it);
			this.toPut = toPut;
		}
		
		@Override
		public Substitution next()
		{
			Substitution s = super.next();
			s.put(toPut);
			return s;
		}
	}
}
