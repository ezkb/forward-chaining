package fr.lirmm.graphik.graal.new_forward_chaining.trigger_applier;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.TriggerApplierException;

public interface TriggerApplier 
{
	AtomSet getFactBase();
	void parallelApply(Rule rule, Substitution sub, AtomSet newFacts) throws TriggerApplierException;
	void directApply(Rule rule, Substitution sub) throws TriggerApplierException;
	void directApply(Rule rule, Substitution sub, AtomSet newFacts) throws TriggerApplierException;
	void init (AtomSet factBase);
}
