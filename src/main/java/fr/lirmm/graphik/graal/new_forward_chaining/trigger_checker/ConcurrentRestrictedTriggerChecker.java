package fr.lirmm.graphik.graal.new_forward_chaining.trigger_checker;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.Semaphore;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Query;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.homomorphism.Homomorphism;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.homomorphism.BacktrackHomomorphism;
import fr.lirmm.graphik.graal.homomorphism.backjumping.BackJumping;
import fr.lirmm.graphik.graal.homomorphism.backjumping.GraphBaseBackJumping;
import fr.lirmm.graphik.graal.homomorphism.bbc.BCC;
import fr.lirmm.graphik.graal.homomorphism.bootstrapper.StarBootstrapper;
import fr.lirmm.graphik.graal.homomorphism.forward_checking.NFC2;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.TriggerCheckerException;
import fr.lirmm.graphik.graal.store.rdbms.RdbmsStore;
import fr.lirmm.graphik.graal.store.rdbms.homomorphism.SqlHomomorphism;

public class ConcurrentRestrictedTriggerChecker extends ConcurrentSemiObliviousTriggerChecker 
{
	LinkedTransferQueue<Homomorphism<? extends Object, ? extends AtomSet>> backtrackSolvers = 
			new LinkedTransferQueue<Homomorphism<? extends Object, ? extends AtomSet>>();
	Map<Rule,ConjunctiveQuery> queries = new ConcurrentHashMap<Rule,ConjunctiveQuery>();
	
	Semaphore lockSQLSolver = new Semaphore(1);
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean check(Rule r, Substitution h, AtomSet factBase, boolean parallel) throws TriggerCheckerException 
	{
		if (!super.check(r, h, factBase, parallel))
			return false;
		
		Homomorphism<ConjunctiveQuery, AtomSet> solver = null;
		
		try 
		{
			solver = 
				(Homomorphism<ConjunctiveQuery, AtomSet>)
				getASolver(getQuery(r), factBase);
			if (solver.exist(getQuery(r), factBase, h)) 
			{
				this.releaseASolver(solver);
				return false;
			}
		} 
		catch (Exception e) 
		{
			throw new TriggerCheckerException("An error occured while the restricted check.", e);
		}
		
		this.releaseASolver(solver);
		return true;
	}
	
	private ConjunctiveQuery getQuery(Rule r)
	{
		ConjunctiveQuery query = queries.get(r);
		
		if (query == null)
		{
			query = DefaultConjunctiveQueryFactory.instance().create(r.getHead());
			queries.put(r, query);
		}
		
		return query;
	}
	
	private Homomorphism<? extends Object, ? extends AtomSet> 
		getASolver(Query q, AtomSet a) throws InterruptedException
	{
		Homomorphism<? extends Object, ? extends AtomSet> solver = null;
		
		if (q instanceof ConjunctiveQuery && a instanceof RdbmsStore)
		{
			lockSQLSolver.acquire();
			solver = (Homomorphism<? extends Object, ? extends AtomSet>) 
					SqlHomomorphism.instance();
		}
		else 
		{
			backtrackSolvers.poll();
			
			if (solver == null)
			{
				BackJumping bj = new GraphBaseBackJumping();
				solver = (Homomorphism<? extends Object, ? extends AtomSet>) 
						new BacktrackHomomorphism(
						new BCC(bj, true),
						StarBootstrapper.instance(),
						new NFC2(),
						bj);
			}
		}
		
		return solver;
	}
	
	private <T> void releaseASolver (T solver)
	{
		if (solver instanceof BacktrackHomomorphism)
		{
			backtrackSolvers.offer((BacktrackHomomorphism)solver);
		}
		else if (solver instanceof SqlHomomorphism)
		{
			this.lockSQLSolver.release();
		}
	}
}
