package fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.EndOfStepTreatmentException;

public interface EndOfStepTreatment 
{
	AtomSet getFactBase();
	void globalExtend(AtomSet newFacts) throws EndOfStepTreatmentException;
	void applyTreatment(AtomSet newFacts) throws EndOfStepTreatmentException;
	void init (AtomSet factBase);
}
