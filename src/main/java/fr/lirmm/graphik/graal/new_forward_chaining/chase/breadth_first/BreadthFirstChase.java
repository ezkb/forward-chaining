package fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.halting_condition.BFCHaltingCondition;
import fr.lirmm.graphik.graal.new_forward_chaining.halting_condition.HaltingCondition;

public interface BreadthFirstChase extends Chase
{
	void execute() throws ChaseException;
	void execute(HaltingCondition condition) throws ChaseException;
	void next() throws ChaseException;
	boolean hasNext() throws ChaseException ;
	void addBFCHaltingCondition(BFCHaltingCondition bFCHaltingCondition);
	int getStep();
	AtomSet getFactBase();
	RuleSet getRuleBase();
}
