package fr.lirmm.graphik.graal.new_forward_chaining.halting_condition;

import fr.lirmm.graphik.graal.new_forward_chaining.chase.Chase;

abstract public class AbstractHaltingCondition implements HaltingCondition 
{
	private Chase chase;
	
	@Override
	public void init(Chase c) 
	{
		chase = c;
	}

	public Chase getChase() 
	{
		return chase;
	}

}