package fr.lirmm.graphik.graal.new_forward_chaining.exception;

public class RuleApplierException extends Exception 
{
	private static final long serialVersionUID = -4506482288837542815L;

	public RuleApplierException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public RuleApplierException(String message) 
	{
		super(message);
	}
	
	public RuleApplierException(Throwable e) 
	{
		super(e);
	}
}
