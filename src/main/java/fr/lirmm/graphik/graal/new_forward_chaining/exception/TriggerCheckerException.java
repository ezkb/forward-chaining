package fr.lirmm.graphik.graal.new_forward_chaining.exception;

public class TriggerCheckerException extends Exception
{
	private static final long serialVersionUID = 7289543994751949498L;

	public TriggerCheckerException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public TriggerCheckerException(String message) 
	{
		super(message);
	}
	
	public TriggerCheckerException(Throwable e) 
	{
		super(e);
	}
}
