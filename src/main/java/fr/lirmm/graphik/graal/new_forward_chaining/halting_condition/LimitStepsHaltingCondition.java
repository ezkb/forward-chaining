package fr.lirmm.graphik.graal.new_forward_chaining.halting_condition;

public class LimitStepsHaltingCondition extends AbstractBFCHaltingCondition {
	long steps;
	
	public LimitStepsHaltingCondition(long steps) 
	{
		this.steps = steps;
	}

	@Override
	public boolean isFinished() 
	{
		return this.getChase().getStep() >= steps;
	}
}
