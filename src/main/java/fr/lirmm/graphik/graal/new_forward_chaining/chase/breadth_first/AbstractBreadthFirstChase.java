package fr.lirmm.graphik.graal.new_forward_chaining.chase.breadth_first;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.Predicate;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.core.ruleset.IndexedByBodyPredicatesRuleSet;
import fr.lirmm.graphik.graal.new_forward_chaining.chase.AbstractChase;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.DefaultEndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.end_of_step_treatment.EndOfStepTreatment;
import fr.lirmm.graphik.graal.new_forward_chaining.exception.ChaseException;
import fr.lirmm.graphik.graal.new_forward_chaining.halting_condition.BFCHaltingCondition;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.DefaultRuleApplier;
import fr.lirmm.graphik.graal.new_forward_chaining.rule_applier.RuleApplier;

public abstract class AbstractBreadthFirstChase extends AbstractChase implements BreadthFirstChase
{

	// PROPERTIES

	private List<BFCHaltingCondition> bFCHaltingConditions;
	private AtomSet precNewFacts;
	private AtomSet newFacts;
	private IndexedByBodyPredicatesRuleSet indexedRuleSet;
	private Set<Rule> rulesToCheck;

	int step = 0;

	// CONSTRUCTORS
	public AbstractBreadthFirstChase(Iterable<Rule> ruleBase, AtomSet factBase,
			RuleApplier ra, EndOfStepTreatment endOfStepTreatment) throws ChaseException 
	{
		super(ruleBase, factBase);
		
		this.setEndOfStepTreatment(endOfStepTreatment);
		this.setRuleApplier(ra);
		
		init();
	}

	public AbstractBreadthFirstChase(Iterable<Rule> ruleBase, AtomSet factBase) throws ChaseException 
	{
		this(ruleBase, factBase, 
				new DefaultRuleApplier(),
				new DefaultEndOfStepTreatment());
	}
	
	protected void init() throws ChaseException 
	{
		
		bFCHaltingConditions = new LinkedList<BFCHaltingCondition>();
		this.newFacts = new DefaultInMemoryGraphStore();
		this.precNewFacts = new DefaultInMemoryGraphStore();

		try 
		{
			this.precNewFacts.addAll(this.getFactBase().iterator());
		} 
		catch (Exception e)
		{
			throw new ChaseException("An error occured during the execution of the forward chaining, during the step : "+this.getStep()+".", e);
		}
		initRulesToCheck ();
	}
	
	protected void initRulesToCheck () throws ChaseException
	{
		rulesToCheck = new HashSet<Rule>();
		indexedRuleSet = new IndexedByBodyPredicatesRuleSet();
		indexedRuleSet.addAll(this.getRuleBase().iterator());
		this.rulesToCheck.addAll(indexedRuleSet);
	}

	@Override
	public boolean hasNext() throws ChaseException 
	{
		try 
		{
			if (this.precNewFacts.isEmpty() && step != 0)
				return false;
			
			if (!super.hasNext())
				return false;
			
			for (BFCHaltingCondition halt : this.bFCHaltingConditions)
			{
				if (halt.isFinished())
					return false;
			}
		}
		catch (Exception e)
		{
			throw new ChaseException("An error occured during the execution of the forward chaining, during the step : "+this.getStep()+".", e);
		}
		
		return true;
	}

	protected void updateRulesToCheck() throws ChaseException 
	{
		rulesToCheck = new HashSet<Rule>();
		try 
		{
			for (Predicate p : getNewFacts().getPredicates()) 
			{
				Iterator<Rule> itR = indexedRuleSet.getRulesByBodyPredicate(p).iterator(); 
				while (itR.hasNext())
				{
					rulesToCheck.add(itR.next());
				}
			}
		} 
		catch (Exception e)
		{
			throw new ChaseException("An error occured during the execution of the forward chaining, during the step : "+this.getStep()+".", e);
		}
	}

	// GETTERS AND SETTERS
	public Set<Rule> getRulesToCheck() 
	{
		return rulesToCheck;
	}
	
	protected AtomSet getNewFacts() 
	{
		return newFacts;
	}

	public AtomSet getPrecNewFacts() 
	{
		return precNewFacts;
	}

	public int getStep() 
	{
		return step;
	}

	public void addBFCHaltingCondition(BFCHaltingCondition bFCHaltingCondition) 
	{
		bFCHaltingConditions.add(bFCHaltingCondition);
		bFCHaltingCondition.init(this);
	}

	protected void setbFCHaltingConditions(List<BFCHaltingCondition> bFCHaltingConditions) 
	{
		this.bFCHaltingConditions = bFCHaltingConditions;
		for (BFCHaltingCondition hc : bFCHaltingConditions)
		{
			hc.init(this);
		}
	}

	protected void setPrecNewFacts(AtomSet precNewFacts) 
	{
		this.precNewFacts = precNewFacts;
	}

	protected void setNewFacts(AtomSet NewFacts) 
	{
		this.newFacts = NewFacts;
	}

	@Override
	public void setRuleBase(RuleSet ruleBase) throws ChaseException 
	{
		super.setRuleBase(ruleBase);
		this.getRuleApplier().init(this);
		this.initRulesToCheck();
	}
}
