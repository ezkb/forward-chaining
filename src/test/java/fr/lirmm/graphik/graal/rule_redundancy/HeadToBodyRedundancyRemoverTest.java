package fr.lirmm.graphik.graal.rule_redundancy;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.api.io.ParseException;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;

public class HeadToBodyRedundancyRemoverTest {
	
	private static final String rule1 = "s(X,Y), s(Y,Z) :- q(X),t(X,Z).";
	private static final String ruleBodyRedundancy = "s(X,Y), s(Y,Z), s(X,T) :- q(X),t(X,Z).";
	private static final String ruleBodyRedundancy2 = "q(X,T) :- q(X, Y), q(Z,T), q(X, Y).";
	private static final String ruleRedundancyFreeze = "p(Y,Z), p(U,Z), p(V,U) :- p(X, Y).";
	
	@Test
	public void headToBodyLoad() throws RuleRedundancyException, ParseException 
	{
		IntraRuleRedundancyRemover h = new IntraRuleRedundancyRemover(rule1);
		Assert.assertTrue(h.getRule().equals(DlgpParser.parseRule(rule1)));
	}
	
	@Test
	public void SimplifyBodyTest() throws RuleRedundancyException, ParseException, AtomSetException
	{
		IntraRuleRedundancyRemover h = new IntraRuleRedundancyRemover(ruleBodyRedundancy2);
		Assert.assertTrue(h.getRule().getBody().size() == (DlgpParser.parseRule(ruleBodyRedundancy2)).getBody().size());
	}
	
	@Test
	public void SimplifyHeadTest() throws AtomSetException, HomomorphismException, IOException, RuleRedundancyException {
		
		IntraRuleRedundancyRemover h = new IntraRuleRedundancyRemover(ruleBodyRedundancy);
		Assert.assertTrue(h.getRule().getHead().size() != (DlgpParser.parseRule(ruleBodyRedundancy)).getHead().size());
		
	}
	
	@Test
	public void SimplifyTestFreeze() throws AtomSetException, HomomorphismException, IOException, RuleRedundancyException {
		
		IntraRuleRedundancyRemover h = new IntraRuleRedundancyRemover(ruleRedundancyFreeze);
		Rule rule = DlgpParser.parseRule(ruleRedundancyFreeze);
		
		Assert.assertTrue(h.getRule().getBody().size() == rule.getBody().size());
		Assert.assertTrue(h.getRule().getHead().size() != rule.getHead().size());
		
	}
	
	
	

}