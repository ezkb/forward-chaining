package fr.lirmm.graphik.graal.rule_redundancy;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.util.stream.IteratorException;

public class RuleSetRedundancyTest {

	private static final String dlgpRules1 = "./src/test/resources/rulesDlgp.dlp";
	private static final String ruleThenBR = "./src/test/resources/ruleThenBR.dlp";
	private static final String dlgpRules2 = "./src/test/resources/rulesDlgp2.dlp";
	private static final String dlgpFile1 = "./src/test/resources/simple.dlp";
	private DlgpParser parser; 
	
	@Test
	public void dlgpRuleFileLoadTest() throws IteratorException, AtomSetException, HomomorphismException, IOException, RuleRedundancyException
	{
		InterRuleRedundancyRemover interRuleRedundancyRemover = new InterRuleRedundancyRemover(dlgpRules1);
		
		parser = new DlgpParser(new File(dlgpRules1));
		int i = 0;
		while (parser.hasNext()) {
			++i;
			parser.next();
		}
		parser.close();
		System.out.println(interRuleRedundancyRemover.getRuleSet());
		System.out.println(i);
		Assert.assertTrue(interRuleRedundancyRemover.getRuleSet().size() != i);
		
	}
	
//	@Test
//	public void dlgpFileLoadTest() throws ParseException, FileNotFoundException{
//		
//		RuleSetRedundancy ruleSetRedundancy = new RuleSetRedundancy(dlgpFile1);
//		
//		Assert.assertEquals(ruleSetRedundancy.getRuleSet().size(), 9);
//		
//	}
	
	@Test
	public void redundance1Test() throws IteratorException, AtomSetException, HomomorphismException, IOException, RuleRedundancyException
	{
		InterRuleRedundancyRemover interRuleRedundancyRemover = new InterRuleRedundancyRemover(ruleThenBR);
		
		parser = new DlgpParser(new File(ruleThenBR));
		int i = 0;
		while (parser.hasNext()) {
			++i;
			parser.next();
		}
		parser.close();
		System.out.println(interRuleRedundancyRemover.getRuleSet());
		System.out.println(i);
		Assert.assertTrue(interRuleRedundancyRemover.getRuleSet().size() != i);
		
	}
	
}