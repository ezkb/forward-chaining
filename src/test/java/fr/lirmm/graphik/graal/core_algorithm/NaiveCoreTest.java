package fr.lirmm.graphik.graal.core_algorithm;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.api.io.ParseException;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.core_algorithm.Core;
import fr.lirmm.graphik.graal.core_algorithm.NaiveCore;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.util.stream.CloseableIterator;



public class NaiveCoreTest 
{
	private static final String stringAtomSet = "[f2] p(X), t(X,a,b), s(a,z). p(Y), t(X,a,b), relatedTo(Y,z).";

	@Test
	public void NaiveCoreCalcul() throws ParseException, AtomSetException, HomomorphismException, IOException {
		
		CloseableIterator<Atom> it = DlgpParser.parseAtomSet(stringAtomSet); 
		AtomSet atomSet = new DefaultInMemoryGraphStore();
		
		while(it.hasNext())
			atomSet.add((Atom)it.next());
		
		Core core = new NaiveCore();
		
		
		Assert.assertTrue(atomSet.size() != core.getCore(DlgpParser.parseAtomSet(stringAtomSet)).size());
	}

}
